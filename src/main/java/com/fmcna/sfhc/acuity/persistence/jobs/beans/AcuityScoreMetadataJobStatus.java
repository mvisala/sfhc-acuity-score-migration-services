package com.fmcna.sfhc.acuity.persistence.jobs.beans;

import java.io.Serializable;
import java.sql.Clob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fmcna.sfhc.acuity.beans.enums.ResourceJobStatusEnum;

@Entity
@Table(name = "ACUITY_SCORE_METADATA_JOB_STATUS", schema = "SFHC")
@SequenceGenerator(name = "pkGenerator", sequenceName = "ACUITY_SCORE_METADATA_JOB_STATUS_SEQ", initialValue = 1, allocationSize = 1)
public class AcuityScoreMetadataJobStatus implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pkGenerator")
	@Column(name = "JOB_ID")
    private Long jobId;

	public AcuityScoreMetadataJobStatus() {

	}
	
	@Column(name = "ACUITY_ID")
    private Long acuityId;

	@Column(name = "mrn")
	private Long mrn;
	
	@Column(name = "PREDICTION_DATE")
	@Temporal(TemporalType.DATE)
	private Date predictionDate;
	
	@Column(name = "CREATED_DTTM")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@Column(name = "MODIFIED_DTTM")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedDate;
	
	@Column(name = "JOB_TYPE")
    private String jobType;

	@Column(name = "JOB_STATUS")
	private String jobStatus;
	
	@Column(name = "REQUEST_JSON")
	private Clob requestJson;
	
	@Column(name = "FHIR_ID")
	private String fhirId;
	
	@Column(name = "SF_ID")
	private String sfId;
	
	@Column(name = "RETRY_COUNT")
    private Integer retryCount = 0;

	@Column(name = "FAILURE_REASON")
	private String failureReason;

	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public Long getAcuityId() {
		return acuityId;
	}

	public void setAcuityId(Long acuityId) {
		this.acuityId = acuityId;
	}

	public Long getMrn() {
		return mrn;
	}

	public void setMrn(Long mrn) {
		this.mrn = mrn;
	}
	
	public Date getPredictionDate() {
		return predictionDate;
	}

	public void setPredictionDate(Date predictionDate) {
		this.predictionDate = predictionDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Clob getRequestJson() {
		return requestJson;
	}

	public void setRequestJson(Clob requestJson) {
		this.requestJson = requestJson;
	}

	public String getFhirId() {
		return fhirId;
	}

	public void setFhirId(String fhirId) {
		this.fhirId = fhirId;
	}

	public String getSfId() {
		return sfId;
	}

	public void setSfId(String sfId) {
		this.sfId = sfId;
	}

	public Integer getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}

	public String getFailureReason() {
		return failureReason;
	}

	public void setFailureReason(String failureReason) {
		this.failureReason = failureReason;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(ResourceJobStatusEnum resourceJobStatusEnum) {
		this.jobStatus = resourceJobStatusEnum.getStatus();
	}
}