package com.fmcna.sfhc.acuity.persistence.acuity.repo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class AcuityScoreRepository {
	
	@Autowired
	private Connection acuityConnection;
	
	@Value("${spring.acuitydatasource.schema}")
	private String schemaName;
	
	private static final Logger logger = LoggerFactory.getLogger(AcuityScoreRepository.class);
	
	public ResultSet getSocialWorkerModifiedAcuityScores(LocalDate predictionDate) {
		Date date= null;
		ResultSet resultSet = null;
		PreparedStatement ps = null;
		try {
			date = java.sql.Date.valueOf(predictionDate);
			ps = acuityConnection.prepareStatement(
					"SELECT a.mrn, a.prediction_date, a.totalswpoints, a.swacuitygroup, a.OLD_VAL FROM " + 
					"(SELECT mrn, prediction_date, totalswpoints, swacuitygroup, LEAD (swacuitygroup) " + 
					"OVER (PARTITION BY mrn ORDER BY prediction_date DESC) AS OLD_VAL FROM " + schemaName+ ".prd_mdl_pred038_acuitysw" +
					") a WHERE a.prediction_date = ? AND (a.swacuitygroup <> a.OLD_VAL OR a.OLD_VAL IS NULL) ORDER BY a.mrn desc",
					ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			
			//ps.setDate(1, java.sql.Date.valueOf(java.time.LocalDate.now()));
			ps.setDate(1, date);
			//ps.setDate(1, java.sql.Date.valueOf("2019-12-16"));  //2019-12-16
			logger.info("Fetching Modified Socialworker Acuity Scores ResultSet for date ::: " + date);
			resultSet = ps.executeQuery();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resultSet;
	}
	
	public ResultSet getDieticianModifiedAcuityScores(LocalDate predictionDate) {
		Date date= null;
		ResultSet resultSet = null;
		PreparedStatement ps = null;
		try {
			date = java.sql.Date.valueOf(predictionDate);
			ps = acuityConnection.prepareStatement(
					"SELECT a.mrn, a.prediction_date, a.total_rd_acuity_score, a.OLD_VAL FROM " +
					"(SELECT mrn, total_rd_acuity_score, LEAD (total_rd_acuity_score) " +
					"OVER (PARTITION BY mrn ORDER BY prediction_date DESC) AS OLD_VAL, prediction_date " +
					"FROM "+schemaName+ ".prd_mdl_pred037_acuityrd) a " +
					"WHERE a.prediction_date = ? AND (a.total_rd_acuity_score <> a.OLD_VAL OR a.OLD_VAL IS NULL)",
					ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setDate(1, date);
			logger.info("Fetching Modified Dietician Acuity Scores ResultSet for date ::: " + date);
			resultSet = ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return resultSet;
	}
	
	public ResultSet getSocialWorkerAcuityScoreMetadata(LocalDate predictionDate) {
		Date date= null;
		ResultSet resultSet = null;
		PreparedStatement ps = null;
		try {
			date = java.sql.Date.valueOf(predictionDate);
			ps = acuityConnection.prepareStatement(
					"SELECT a.mrn, a.prediction_date, a.totalswpoints, a.swacuitygroup " +
					"FROM "+schemaName+ ".prd_mdl_pred038_acuitysw a WHERE a.prediction_date = ? ",
					ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setDate(1, date);
			logger.info("Fetching Socialworker Acuity Metadata ResultSet for date ::: " + date);
			resultSet = ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resultSet;
	}
	
	public ResultSet getDieticianAcuityScoreMetadata(LocalDate predictionDate) {
		Date date= null;
		ResultSet resultSet = null;
		PreparedStatement ps = null;
		try {
			date = java.sql.Date.valueOf(predictionDate);
			ps = acuityConnection.prepareStatement(
					"SELECT a.mrn, a.prediction_date, a.total_rd_acuity_score " +
					"FROM " +schemaName+ ".prd_mdl_pred037_acuityrd a WHERE a.prediction_date = ?",
					ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setDate(1, date);
			logger.info("Fetching Dietician Acuity Metadata ResultSet for date ::: " + date);
			resultSet = ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resultSet;
	}
	
	public ResultSet getSocialWorkerModifiedAcuityScoresByMrnList(LocalDate predictionDate, String mrnList) {
		Date date= null;
		ResultSet resultSet = null;
		PreparedStatement ps = null;
		try {
			date = java.sql.Date.valueOf(predictionDate);
			ps = acuityConnection.prepareStatement(
					"SELECT a.mrn, a.prediction_date, a.totalswpoints, a.swacuitygroup, a.OLD_VAL FROM " + 
					"(SELECT mrn, prediction_date, totalswpoints, swacuitygroup, LEAD (swacuitygroup) " + 
					"OVER (PARTITION BY mrn ORDER BY prediction_date DESC) AS OLD_VAL " + 
					"FROM " +schemaName+ ".prd_mdl_pred038_acuitysw) a WHERE a.prediction_date = ? AND a.mrn in (" + mrnList + ")" +
					"AND (a.swacuitygroup <> a.OLD_VAL OR a.OLD_VAL IS NULL) ORDER BY a.mrn desc",
					ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setDate(1, date);
			logger.info("Fetching Modified Socialworker Acuity Scores ResultSet for date ::: " + date);
			resultSet = ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resultSet;
	}
	
	public ResultSet getDieticianModifiedAcuityScoresByMrnList(LocalDate predictionDate, String mrnList) {
		Date date= null;
		ResultSet resultSet = null;
		PreparedStatement ps = null;
		try {
			date = java.sql.Date.valueOf(predictionDate);
			ps = acuityConnection.prepareStatement(
					"SELECT a.mrn, a.prediction_date, a.total_rd_acuity_score, a.OLD_VAL FROM " +
					"(SELECT mrn, total_rd_acuity_score, LEAD (total_rd_acuity_score) " +
					"OVER (PARTITION BY mrn ORDER BY prediction_date DESC) AS OLD_VAL, prediction_date " +
					"FROM " +schemaName+ ".prd_mdl_pred037_acuityrd) a " +
					"WHERE a.prediction_date = ? AND a.mrn in (" + mrnList + ") " +
					"AND (a.total_rd_acuity_score <> a.OLD_VAL OR a.OLD_VAL IS NULL)",
					ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setDate(1, date);
			logger.info("Fetching Modified Dietician Acuity Scores ResultSet for date ::: " + date);
			resultSet = ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return resultSet;
	}
	
	public ResultSet getSocialWorkerAcuityScoreMetadataByMrnList(LocalDate predictionDate, String mrnList) {
		Date date= null;
		ResultSet resultSet = null;
		PreparedStatement ps = null;
		try {
			date = java.sql.Date.valueOf(predictionDate);
			ps = acuityConnection.prepareStatement(
					"SELECT a.mrn, a.prediction_date, a.totalswpoints, a.swacuitygroup " +
					"FROM " +schemaName+ ".prd_mdl_pred038_acuitysw a " +
					"WHERE a.prediction_date = ? AND a.mrn in (" + mrnList + ")",
					ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setDate(1, date);
			logger.info("Fetching Socialworker Acuity Metadata ResultSet for date ::: " + date);
			resultSet = ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resultSet;
	}
	
	public ResultSet getDieticianAcuityScoreMetadataByMrnList(LocalDate predictionDate, String mrnList) {
		Date date= null;
		ResultSet resultSet = null;
		PreparedStatement ps = null;
		try {
			date = java.sql.Date.valueOf(predictionDate);
			ps = acuityConnection.prepareStatement(
					"SELECT a.mrn, a.prediction_date, a.total_rd_acuity_score " +
					"FROM " +schemaName+ ".prd_mdl_pred037_acuityrd a " +
					"WHERE a.prediction_date = ? AND a.mrn in (" + mrnList + ")",
					ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setDate(1, date);
			logger.info("Fetching Modified Dietician Acuity Scores ResultSet for date ::: " + date);
			resultSet = ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resultSet;
	}
}
