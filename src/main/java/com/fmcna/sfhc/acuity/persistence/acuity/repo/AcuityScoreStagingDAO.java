package com.fmcna.sfhc.acuity.persistence.acuity.repo;

import java.sql.ResultSet;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AcuityScoreStagingDAO {

	@Autowired
	private AcuityScoreRepository acuityScoreRepository;
	
	public ResultSet getSocialWorkerModifiedAcuityScores(LocalDate predictionDate) {
		return acuityScoreRepository.getSocialWorkerModifiedAcuityScores(predictionDate);
	}
	
	public ResultSet getDieticianModifiedAcuityScores(LocalDate predictionDate) {
		return acuityScoreRepository.getDieticianModifiedAcuityScores(predictionDate);
	}
	
	public ResultSet getSocialWorkerAcuityScoreMetadata(LocalDate predictionDate) {
		return acuityScoreRepository.getSocialWorkerAcuityScoreMetadata(predictionDate);
	}
	
	public ResultSet getDieticianAcuityScoreMetadata(LocalDate predictionDate) {
		return acuityScoreRepository.getDieticianAcuityScoreMetadata(predictionDate);
	}
	
	public ResultSet getSocialWorkerModifiedAcuityScoresByMrnList(LocalDate predictionDate, String mrnList) {
		return acuityScoreRepository.getSocialWorkerModifiedAcuityScoresByMrnList(predictionDate, mrnList);
	}
	
	public ResultSet getDieticianModifiedAcuityScoresByMrnList(LocalDate predictionDate, String mrnList) {
		return acuityScoreRepository.getDieticianModifiedAcuityScoresByMrnList(predictionDate, mrnList);
	}
	
	public ResultSet getSocialWorkerAcuityScoreMetadataByMrnList(LocalDate predictionDate, String mrnList) {
		return acuityScoreRepository.getSocialWorkerAcuityScoreMetadataByMrnList(predictionDate, mrnList);
	}
	
	public ResultSet getDieticianAcuityScoreMetadataByMrnList(LocalDate predictionDate, String mrnList) {
		return acuityScoreRepository.getDieticianAcuityScoreMetadataByMrnList(predictionDate, mrnList);
	}
}