package com.fmcna.sfhc.acuity.persistence.jobs.repo;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fmcna.sfhc.acuity.persistence.jobs.beans.AcuityScoreMetadataJobStatus;

@Repository 
public interface AcuityScoreMetadataRepository extends JpaRepository<AcuityScoreMetadataJobStatus, Long> {
	
	@Query("SELECT j FROM AcuityScoreMetadataJobStatus j WHERE j.jobId = ?1") 
    List<AcuityScoreMetadataJobStatus> getSocialWorkerAcuityScoreByJobId(Long jobId);
	
	@Query("SELECT j FROM AcuityScoreMetadataJobStatus j WHERE j.jobStatus in ?1 and j.jobType = ?2") 
    List<AcuityScoreMetadataJobStatus> getAcuityScoreMetadataByStatus(List<String> jobStatus, String jobType);
	
	@Query("SELECT j FROM AcuityScoreMetadataJobStatus j WHERE j.jobStatus in ?1 and j.jobType = ?2 and j.createdDate = ?3") 
    List<AcuityScoreMetadataJobStatus> getAcuityScoreMetadataByStatusTypeDate(List<String> jobStatus, String jobType, Date createdDate);
	
	@Modifying
	@Transactional
	@Query("UPDATE AcuityScoreMetadataJobStatus j SET j.jobStatus = ?1, j.fhirId = ?2, j.modifiedDate = ?3 WHERE j.mrn = ?4 and j.jobId = ?5")	
	Integer updateAcuityScoreMetadataByFhirId(String jobStatus, String fhirId, Date modifiedDate, Long mrn, Long jobId);
	
	@Modifying
	@Transactional
	@Query("UPDATE AcuityScoreMetadataJobStatus j SET j.jobStatus = ?1, j.modifiedDate = ?2 WHERE j.fhirId = ?3 and j.predictionDate = ?4 ")
	Integer updateAcuityScoreMetadataJobStatusByFhirStatus(String jobStatus, Date modifiedDate, String fhirId, Date predictionDate);

	@Modifying
	@Transactional
	@Query("UPDATE AcuityScoreMetadataJobStatus j SET j.jobStatus = ?1, j.sfId = ?2, j.modifiedDate = ?3 WHERE j.jobId = ?4")	
	Integer updateAcuityScoreMetadataJobStatusBySfStatus(String jobStatus, String sfId, Date modifiedDate, Long jobId);

	@Modifying
	@Transactional
	@Query("UPDATE AcuityScoreMetadataJobStatus j SET j.jobStatus = ?1, j.failureReason = ?2, j.modifiedDate = ?3 WHERE j.jobId = ?4")	
	Integer updateAcuityScoreMetadataJobStatusBySfFailureStatus(String jobStatus, String failureReason, Date modifiedDate, Long jobId);

	@Modifying
	@Transactional
	@Query("UPDATE AcuityScoreMetadataJobStatus j SET j.jobStatus = ?1, j.modifiedDate = ?2 WHERE j.jobId in ?3")	
	Integer updateAcuityScoreMetadataJobStatusByJobStatus(String jobStatus, Date modifiedDate, List<Long> jobIds);
}