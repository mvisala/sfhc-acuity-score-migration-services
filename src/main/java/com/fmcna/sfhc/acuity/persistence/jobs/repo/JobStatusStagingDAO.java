package com.fmcna.sfhc.acuity.persistence.jobs.repo;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fmcna.sfhc.acuity.persistence.jobs.beans.AcuityScoreMigrationStatus;
import com.fmcna.sfhc.acuity.persistence.jobs.beans.AcuityScoreMetadataJobStatus;

@Component
public class JobStatusStagingDAO {

	@Autowired
	private AcuityScoreMigrationStatusRepository migrationRepository;
	
	@Autowired
	private AcuityScoreMetadataRepository metadataRepository;
	
	public AcuityScoreMigrationStatus saveMigrationJob(AcuityScoreMigrationStatus migrationJob) {

		return migrationRepository.save(migrationJob);
	}
	
	public AcuityScoreMetadataJobStatus saveAcuityScoreMetadata(AcuityScoreMetadataJobStatus swJob) {

		return metadataRepository.save(swJob);
	}
	
	public List<AcuityScoreMetadataJobStatus> saveAcuityScoreMetadataList(List<AcuityScoreMetadataJobStatus> swJobList) {

		return metadataRepository.saveAll(swJobList);
	}
	
	public List<AcuityScoreMetadataJobStatus> getAcuityScoreMetadataByStatus(List<String> jobStatus, String jobType) {
		
		return metadataRepository.getAcuityScoreMetadataByStatus(jobStatus, jobType);
	}
	
	public List<AcuityScoreMetadataJobStatus> getAcuityScoreMetadataByStatusTypeDate(List<String> jobStatus, String jobType, Date createdDate) {
		
		return metadataRepository.getAcuityScoreMetadataByStatusTypeDate(jobStatus, jobType, createdDate);
	}
	
	public Integer updateAcuityScoreMetadataByFhirId(String jobStatus, String fhirId, Date modifiedDate, Long mrn, Long jobId) {
		
		return metadataRepository.updateAcuityScoreMetadataByFhirId(jobStatus, fhirId, modifiedDate, mrn, jobId);
	}
	
	public Integer updateAcuityScoreMetadataJobStatusByFhirStatus(String jobStatus, Date modifiedDate, String fhirId, Date predictionDate) {
		
		return metadataRepository.updateAcuityScoreMetadataJobStatusByFhirStatus(jobStatus, modifiedDate, fhirId, predictionDate);
	}

	public Integer updateAcuityScoreMetadataJobStatusBySfStatus(String jobStatus, String sfId, Date modifiedDate, Long jobId) {
		
		return metadataRepository.updateAcuityScoreMetadataJobStatusBySfStatus(jobStatus, sfId, modifiedDate, jobId);
	}
	
	public Integer updateAcuityScoreMetadataJobStatusBySfFailureStatus(String jobStatus, String failureReason, Date modifiedDate, Long jobId) {
		
		return metadataRepository.updateAcuityScoreMetadataJobStatusBySfFailureStatus(jobStatus, failureReason, modifiedDate, jobId);
	}

	public Integer updateAcuityScoreMetadataJobStatusByJobStatus(String jobStatus, Date modifiedDate, List<Long> jobIds) {
		
		return metadataRepository.updateAcuityScoreMetadataJobStatusByJobStatus(jobStatus, modifiedDate, jobIds);
	}
}