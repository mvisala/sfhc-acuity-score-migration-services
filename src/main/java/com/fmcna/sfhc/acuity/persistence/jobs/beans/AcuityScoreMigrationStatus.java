package com.fmcna.sfhc.acuity.persistence.jobs.beans;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ACUITY_SCORE_MIGRATION_STATUS", schema = "SFHC")
@SequenceGenerator(name = "acuityMigrationPkGenerator", sequenceName = "ACUITY_SCORE_MIGRATION_STATUS_SEQ", initialValue = 1, allocationSize = 1)
public class AcuityScoreMigrationStatus implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "acuityMigrationPkGenerator")
	@Column(name = "ACUITY_ID")
    private Long acuityId;
	
	public AcuityScoreMigrationStatus() {

	}
	
	@Column(name = "ACUITY_TYPE")
    private String acuityType;

	@Column(name = "CREATED_DTTM")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@Column(name = "EXTRACT_COUNT")
    private Integer extractCount;
	

	public Long getAcuityId() {
		return acuityId;
	}

	public void setAcuityId(Long acuityId) {
		this.acuityId = acuityId;
	}
	
	public String getAcuityType() {
		return acuityType;
	}

	public void setAcuityType(String acuityType) {
		this.acuityType = acuityType;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public Integer getExtractCount() {
		return extractCount;
	}

	public void setExtractCount(Integer extractCount) {
		this.extractCount = extractCount;
	}
}