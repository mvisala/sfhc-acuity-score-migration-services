package com.fmcna.sfhc.acuity.persistence.jobs.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fmcna.sfhc.acuity.persistence.jobs.beans.AcuityScoreMigrationStatus;

@Repository 
public interface AcuityScoreMigrationStatusRepository extends JpaRepository<AcuityScoreMigrationStatus, Long> {
	
	@Query("SELECT j FROM AcuityScoreMigrationStatus j WHERE j.acuityId = ?1") 
    List<AcuityScoreMigrationStatus> getAcuityScoreMigrationStatusByJobId(Long acuityId);

}