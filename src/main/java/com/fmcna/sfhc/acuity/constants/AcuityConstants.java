package com.fmcna.sfhc.acuity.constants;

import java.text.SimpleDateFormat;

public class AcuityConstants {

	public final static String FMC_ACUITY_TYPE_SW_RESOURCE = "SW";
	public final static String FMC_ACUITY_TYPE_RD_RESOURCE = "RD";
	
	public final static String URN_OID = "urn:oid:";
	public final static String LOINC_SYSTEMNAME = "LOINC";
	public final static String DATEFORMAT_YYYYMMDD = "yyyyMMdd";
	public final static String DATEFORMAT_YYYYMMDDHHMMZ = "yyyyMMddHHmmZ";
	
	public final static String LOINC_SYSTEM_URL = "http://loinc.org";
	public final static String FMC_ACUITY_REASONCODE_SYSTEM_URL = "http://fkcfhir.org/fhir/cs/FMCAcuityReasonCode";
	
	public static final SimpleDateFormat FHIR_DATEFORMAT_YYYYMMDD = new SimpleDateFormat("'D'MMddyyyy'H'HH'M'mm'S'ss");
	public static final SimpleDateFormat FHIR_DATEFORMAT_YYYYMMDDHHHMMMSss = new SimpleDateFormat("'D'MMddyyyy'H'HH'M'mm'S'ss");
	
	public static final String ACUITY_BUNDLE_IDENTIFIER_SYSTEM_URN = "urn:oid:2.16.840.1.113883.3.7418.29.14";
	public static final String MEASURE_REPORT_IDENTIFIER_SYSTEM_URN = "urn:oid:2.16.840.1.113883.3.7418.41.2";
	
	public static final String BUNDLE_ACUITY_CODE = "Acuity";
	public static final String RESPONSE_NOT_RECEIVED = "No Response received from FHIR.";
	
	public static final SimpleDateFormat SF_DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
	public static final String SF_HEALTHCLOUDGA__EhrPATIENT = "HealthCloudGA__EhrPatient__c";
	
	public static final String mrnListError = "Send valid MRN list with comma as ex: /migratelist/2019-12-16/sw/5000448375,5000085334";
}
