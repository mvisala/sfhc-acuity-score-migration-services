package com.fmcna.sfhc.acuity.beans.enums;

public enum ResourceJobStatusEnum {

	NEW("New"),
	FHIR_SENT("FHIR Sent"),
	FHIR_SUCCESS("FHIR Success"),
	FHIR_FAILED("FHIR Failed"),
	PATIENT_FAILED("Patient Failures"),
	SF_PROCESSING("Salesforce Processing"),
	SF_SUCCESS("Salesforce Success"),
	SF_FAILED("Salesforce Failed"),
	COMPLETE("Complete"),
	FAILED("Failed"),
	IN_PROGRESS("In Progress"),
	RETRY("Retry"),
	NO_PATIENTS("No Patients"),
	UNKNOWN("Unknown");

	private String status;

	private ResourceJobStatusEnum(String status) {
		this.status = status;
	}

	public String getStatus() {
		return this.status;
	}

	public static ResourceJobStatusEnum fromValue(String value) {
		if (value != null) {
			for (ResourceJobStatusEnum status : values()) {
				if (status.getStatus().equals(value)) {
					return status;
				}
			}
		}
		return getDefault();
	}

	private static ResourceJobStatusEnum getDefault() {
		return UNKNOWN;
	}

}
