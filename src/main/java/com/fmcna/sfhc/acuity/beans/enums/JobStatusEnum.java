package com.fmcna.sfhc.acuity.beans.enums;

public enum JobStatusEnum {

	STARTED("Started"),
	IN_PROGRESS("In Progress"),
	COMPLETE("Complete"), 
	FAILED("Failed"), 
	UNKNOWN("Unknown");

	private String status;

	private JobStatusEnum(String status) {
		this.status = status;
	}

	public String getStatus() {
		return this.status;
	}

	public static JobStatusEnum fromValue(String value) {
		if (value != null) {
			for (JobStatusEnum status : values()) {
				if (status.getStatus().equals(value)) {
					return status;
				}
			}
		}
		return getDefault();
	}

	private static JobStatusEnum getDefault() {
		return UNKNOWN;
	}

}
