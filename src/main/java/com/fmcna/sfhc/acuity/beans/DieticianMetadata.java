package com.fmcna.sfhc.acuity.beans;

import java.util.Date;

public class DieticianMetadata {
	private Long mrn;
	private Date predictionDate;
	private Integer totalRdAcuityScore;
	private Long id;
	
	public Long getMrn() {
		return mrn;
	}
	public void setMrn(Long mrn) {
		this.mrn = mrn;
	}
	public Date getPredictionDate() {
		return predictionDate;
	}
	public void setPredictionDate(Date predictionDate) {
		this.predictionDate = predictionDate;
	}
	public Integer getTotalRdAcuityScore() {
		return totalRdAcuityScore;
	}
	public void setTotalRdAcuityScore(Integer totalRdAcuityScore) {
		this.totalRdAcuityScore = totalRdAcuityScore;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

   /* private String reason1d;
    private String reason2d;
    private String reason3d;
    private String reason4d;
    private String reason5d;
    private String reason6d;
    private String reason7d;
    private String reason8d;
    private String reason9d;
    private String reason10d;
    
	public String getReason1d() {
		return reason1d;
	}
	public void setReason1d(String reason1d) {
		this.reason1d = reason1d;
	}
	public String getReason2d() {
		return reason2d;
	}
	public void setReason2d(String reason2d) {
		this.reason2d = reason2d;
	}
	public String getReason3d() {
		return reason3d;
	}
	public void setReason3d(String reason3d) {
		this.reason3d = reason3d;
	}
	public String getReason4d() {
		return reason4d;
	}
	public void setReason4d(String reason4d) {
		this.reason4d = reason4d;
	}
	public String getReason5d() {
		return reason5d;
	}
	public void setReason5d(String reason5d) {
		this.reason5d = reason5d;
	}
	public String getReason6d() {
		return reason6d;
	}
	public void setReason6d(String reason6d) {
		this.reason6d = reason6d;
	}
	public String getReason7d() {
		return reason7d;
	}
	public void setReason7d(String reason7d) {
		this.reason7d = reason7d;
	}
	public String getReason8d() {
		return reason8d;
	}
	public void setReason8d(String reason8d) {
		this.reason8d = reason8d;
	}
	public String getReason9d() {
		return reason9d;
	}
	public void setReason9d(String reason9d) {
		this.reason9d = reason9d;
	}
	public String getReason10d() {
		return reason10d;
	}
	public void setReason10d(String reason10d) {
		this.reason10d = reason10d;
	} */
}