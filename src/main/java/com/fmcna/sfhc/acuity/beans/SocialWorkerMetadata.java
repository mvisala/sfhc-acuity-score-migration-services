package com.fmcna.sfhc.acuity.beans;

import java.util.Date;

public class SocialWorkerMetadata {
	private Long mrn;
	private Date predictionDate;
	private Double totalSwPoints;
	private String swAcuityGroup;
	private Long id;
	
	public Long getMrn() {
		return mrn;
	}
	public void setMrn(Long mrn) {
		this.mrn = mrn;
	}
	public Date getPredictionDate() {
		return predictionDate;
	}
	public void setPredictionDate(Date predictionDate) {
		this.predictionDate = predictionDate;
	}
	public Double getTotalSwPoints() {
		return totalSwPoints;
	}
	public void setTotalSwPoints(Double totalSwPoints) {
		this.totalSwPoints = totalSwPoints;
	}
	public String getSwAcuityGroup() {
		return swAcuityGroup;
	}
	public void setSwAcuityGroup(String swAcuityGroup) {
		this.swAcuityGroup = swAcuityGroup;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}