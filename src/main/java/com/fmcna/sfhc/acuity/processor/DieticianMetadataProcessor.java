package com.fmcna.sfhc.acuity.processor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fmcna.sfhc.acuity.beans.DieticianMetadata;
import com.fmcna.sfhc.acuity.persistence.jobs.beans.AcuityScoreMigrationStatus;
import com.fmcna.sfhc.acuity.services.RepositoryService;
import com.fmcna.sfhc.acuity.services.mapper.FHIRRequestMapper;

@Component
@Scope("prototype")
public class DieticianMetadataProcessor {
	
	@Autowired
	private RepositoryService repositoryService;
	
	@Autowired
	private FHIRRequestMapper fhirRequestMapper;
	
	@Value("${max.records.sentto.fhir}")
	private int fhirMaxLimit;

	private static final Logger logger = LoggerFactory.getLogger(DieticianMetadataProcessor.class);
	
	public int saveDieticianMetadata(ResultSet resultSet) {
		int i = 1;
		int count = 0;
		int extractCount = 0;
		int migrationCount = 0;
		Long acuityId = null;
		logger.info("Processing Acuity Score Metadata for Dietician...");
		
		try {
			resultSet.last();
			extractCount = resultSet.getRow();
			logger.info("Total RD Fetch Size ::: " + extractCount);
			
			acuityId = this.saveMigrationJob("ACUITY_RD", extractCount);
			
			if (acuityId != null) {
				logger.info("Successfully saved Dietician Migration Job with Acuity Id ::: " +acuityId);
				
				resultSet.beforeFirst();
				List<DieticianMetadata> metadataSubList = new ArrayList<>();
				
				while (resultSet.next()) {
					DieticianMetadata metadata = new DieticianMetadata();
					metadata = this.getMetadata(metadata, resultSet);
					metadataSubList.add(metadata);
					
					if (i++ % fhirMaxLimit == 0) {
						List<DieticianMetadata> bundleSubList = metadataSubList;
						logger.info("Processing Metadata subset :::  " + ++count + "  with size ::: " +bundleSubList.size());
						migrationCount = migrationCount + bundleSubList.size();
						fhirRequestMapper.createDieticianBundleMessage(acuityId, metadataSubList);
						//CompletableFuture.supplyAsync(() -> this.sendBundleToFhir(bundleSubList), cachedThreadPool);
						metadataSubList = new ArrayList<>();
					}
				}
				
				if (metadataSubList.size() > 0) {
					List<DieticianMetadata> bundleSubList = metadataSubList;
					logger.info("Processing Metadata subset :::  " + ++count + "  with size ::: " +bundleSubList.size());
					migrationCount = migrationCount + bundleSubList.size();
					fhirRequestMapper.createDieticianBundleMessage(acuityId, metadataSubList);
					metadataSubList = new ArrayList<>();
				}
				
				logger.info("******** Total no. of Dietician Acuity Scores migrated to FHIR ::: " + migrationCount);
			} else {
				logger.error("Unable to save Migration Job for Dietician ");
			}
		} catch (Exception ex) {
			logger.error("Error in DieticianMetadataProcessor:::::saveDieticianMetadata() ==>>>" + ex.getMessage());
			ex.printStackTrace();
		}
		
		return migrationCount;
	}
	
	private DieticianMetadata getMetadata(DieticianMetadata metadata, ResultSet resultSet) {
		try {
			metadata.setMrn(resultSet.getLong("MRN"));  
			//rdMetadata.setMrn(5000448375L); //DEV
			metadata.setPredictionDate(resultSet.getDate("PREDICTION_DATE"));
			metadata.setTotalRdAcuityScore(resultSet.getInt("TOTAL_RD_ACUITY_SCORE"));
		} catch (SQLException e) {
			logger.error("SQLException in DieticianMetadataProcessor:::getMetadata() - Unable to fetch metadata " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("Exception in DieticianMetadataProcessor:::getMetadata() - Unable to fetch metadata " + e.getMessage());
			e.printStackTrace();
		}
		
		return metadata;
	}
	
	private Long saveMigrationJob(String acuityType, int size) {
		Long acuityId = null;
		try {
			logger.info("Saving the Migration Job...........");
			AcuityScoreMigrationStatus migrationJob = new AcuityScoreMigrationStatus();
			migrationJob.setAcuityType(acuityType);
			migrationJob.setExtractCount(size);
			migrationJob.setCreatedDate(new Date());
			migrationJob = repositoryService.saveMigrationJob(migrationJob);
			
			if (migrationJob != null)
				acuityId = migrationJob.getAcuityId();
			}
		catch (Exception ex) {
			logger.error("Error in DieticianMetadataProcessor:::saveMigrationJob() ==>>>" + ex.getMessage());
			ex.printStackTrace();
		}
		return acuityId;
	}
	
	/*private Long saveMetadataJob(Long acuityId, DieticianMetadata metadata) {
		Long jobId = null;
		try {
			AcuityScoreMetadataJobStatus metadataJob = new AcuityScoreMetadataJobStatus();
			metadataJob.setAcuityId(acuityId);
			metadataJob.setJobType("RD_JOB");
			metadataJob.setJobStatus(ResourceJobStatusEnum.NEW);
			metadataJob.setCreatedDate(new Date());
			metadataJob.setMrn(metadata.getMrn());
			metadataJob.setPredictionDate(metadata.getPredictionDate());
			//metadataJob.setMrn(5000448375L);
			metadataJob.setRequestJson(new SerialClob(objectMapper.writeValueAsString(metadata).toString().toCharArray()));
			
			metadataJob = repositoryService.saveAcuityScoreMetadata(metadataJob);
			
			if (metadataJob.getJobId() != null) {
				jobId = metadataJob.getJobId();
			}
		} catch (Exception ex) {
			logger.error("Error in DieticianMetadataProcessor:::::saveMetadataJob() -> " + ex.getMessage());
			ex.printStackTrace();
		}
		
		return jobId;
	} */
}
