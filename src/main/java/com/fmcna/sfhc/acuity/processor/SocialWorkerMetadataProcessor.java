package com.fmcna.sfhc.acuity.processor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fmcna.sfhc.acuity.beans.SocialWorkerMetadata;
import com.fmcna.sfhc.acuity.persistence.jobs.beans.AcuityScoreMigrationStatus;
import com.fmcna.sfhc.acuity.services.RepositoryService;
import com.fmcna.sfhc.acuity.services.mapper.FHIRRequestMapper;

@Component
@Scope("prototype")
public class SocialWorkerMetadataProcessor {
	
	@Autowired
	private RepositoryService repositoryService;
	
	@Autowired
	private FHIRRequestMapper fhirRequestMapper;
	
	//@Autowired
	//private ExecutorService cachedThreadPool;
	
	@Value("${max.records.sentto.fhir}")
	private int fhirMaxLimit;

	private static final Logger logger = LoggerFactory.getLogger(SocialWorkerMetadataProcessor.class);
	
	public int saveSocialWorkerMetadata(ResultSet resultSet) {
		int i = 1;
		int count = 0;
		int extractCount = 0;
		int migrationCount = 0;
		Long acuityId = null;
		logger.info("Processing Acuity Score Metadata for SocialWorker...");
		
		try {
			resultSet.last();
			extractCount = resultSet.getRow();
			logger.info("Total SW Fetch Size ::: " + extractCount);
			
			acuityId = this.saveMigrationJob("ACUITY_SW", extractCount);
			
			if (acuityId != null) {
				logger.info("Successfully saved SocialWorker Migration Job with Acuity Id ::: " +acuityId);
				
				resultSet.beforeFirst();
				List<SocialWorkerMetadata> metadataSubList = new ArrayList<>();
				
				while (resultSet.next()) {
					SocialWorkerMetadata metadata = new SocialWorkerMetadata();
					metadata = this.getMetadata(metadata, resultSet);
					metadataSubList.add(metadata);
					
					if (i++ % fhirMaxLimit == 0) {
						List<SocialWorkerMetadata> bundleSubList = metadataSubList;
						logger.info("Processing Metadata subset :::  " + ++count + "  with size ::: " +bundleSubList.size());
						migrationCount = migrationCount + bundleSubList.size();
						//CompletableFuture.supplyAsync(() -> this.sendBundleToFhir(metadataSubList2), cachedThreadPool);
						fhirRequestMapper.createSocialWorkerBundleMessage(acuityId, metadataSubList);
						metadataSubList = new ArrayList<>();
					}
				} 
				if (metadataSubList.size() > 0) {
					List<SocialWorkerMetadata> bundleSubList = metadataSubList;
					logger.info("Processing Metadata subset :::  " + ++count + "  with size ::: " +bundleSubList.size());
					migrationCount = migrationCount + metadataSubList.size();
					fhirRequestMapper.createSocialWorkerBundleMessage(acuityId, metadataSubList);
					metadataSubList = new ArrayList<>();
				}
				logger.info("********* Total no. of Socialworker Acuity Scores migrated to FHIR ::: " + migrationCount);
			} else {
				logger.error("Unable to save Migration Job for Socialworker ");
			}
		}
		catch (Exception ex) {
			logger.error("Error in SocialWorkerMetadataProcessor:::::saveSocialWorkerMetadata() ==>>> " + ex.getMessage());
			ex.printStackTrace();
		}
		
		return migrationCount;
	}
	
	private SocialWorkerMetadata getMetadata(SocialWorkerMetadata metadata, ResultSet resultSet) {
		try {
			String scoreRangeText = null;
			metadata.setMrn(resultSet.getLong("MRN"));
			//metadata.setMrn(5000602756L); //UAT
			//metadata.setMrn(5000448375L); //DEV
			metadata.setPredictionDate(resultSet.getDate("PREDICTION_DATE"));
			metadata.setTotalSwPoints(resultSet.getDouble("TOTALSWPOINTS"));
			scoreRangeText = resultSet.getString("SWACUITYGROUP");
			if (scoreRangeText != null)
				metadata.setSwAcuityGroup(scoreRangeText.replaceAll("\\d","").trim());
			else 
				logger.error("scoreRangeText is null");
		} catch (SQLException e) {
			logger.error("SQLException in SocialWorkerMetadataProcessor:::getMetadata() - Unable to fetch metadata " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("Exception in SocialWorkerMetadataProcessor:::getMetadata() - Unable to fetch metadata " + e.getMessage());
			e.printStackTrace();
		}
		
		return metadata;
	}
	
	private Long saveMigrationJob(String acuityType, int size) {
		Long acuityId = null;
		try {
			AcuityScoreMigrationStatus migrationJob = new AcuityScoreMigrationStatus();
			migrationJob.setAcuityType(acuityType);
			migrationJob.setExtractCount(size);
			migrationJob.setCreatedDate(new Date());
			migrationJob = repositoryService.saveMigrationJob(migrationJob);
			
			if (migrationJob != null)
				acuityId = migrationJob.getAcuityId();
			}
		catch (Exception ex) {
			logger.error("Error in SocialWorkerMetadataProcessor:::saveMigrationJob() ==>>>" + ex.getMessage());
			ex.printStackTrace();
		}
		return acuityId;
	}
	
	/* private Long saveMetadataJob(Long acuityId, SocialWorkerMetadata metadata, String errorMsg) {
		Long jobId = null;
		try {
			AcuityScoreMetadataJobStatus metadataJob = new AcuityScoreMetadataJobStatus();
			metadataJob.setAcuityId(acuityId);
			metadataJob.setJobType("SW_JOB");
			metadataJob.setJobStatus(ResourceJobStatusEnum.NEW);
			metadataJob.setCreatedDate(new Date());
			metadataJob.setMrn(metadata.getMrn());
			metadataJob.setPredictionDate(metadata.getPredictionDate());
			//metadataJob.setMrn(5000448375L); //DEV-FHIR
			metadataJob.setRequestJson(new SerialClob(objectMapper.writeValueAsString(metadata).toString().toCharArray()));
			
			if (errorMsg != null)
				metadataJob.setFailureReason(errorMsg);
			metadataJob = repositoryService.saveAcuityScoreMetadata(metadataJob);

			if (metadataJob.getJobId() != null) {
				jobId = metadataJob.getJobId();
			}
		} catch (Exception ex) {
			logger.error("Error in SocialWorkerMetadataProcessor:::::saveMetadataJob() -> " + ex.getMessage());
			ex.printStackTrace();
		}
		return jobId;
	}
	
	private AcuityScoreMetadataJobStatus getMetadataJob(Long acuityId, SocialWorkerMetadata metadata, String errorMsg) {
		AcuityScoreMetadataJobStatus metadataJob = null;
		try {
			metadataJob = new AcuityScoreMetadataJobStatus();
			metadataJob.setAcuityId(acuityId);
			metadataJob.setJobType("SW_JOB");
			metadataJob.setJobStatus(ResourceJobStatusEnum.NEW);
			metadataJob.setCreatedDate(new Date());
			metadataJob.setMrn(metadata.getMrn());
			metadataJob.setPredictionDate(metadata.getPredictionDate());
			//metadataJob.setMrn(5000448375L); //DEV-FHIR
			metadataJob.setRequestJson(new SerialClob(objectMapper.writeValueAsString(metadata).toString().toCharArray()));
			
			if (errorMsg != null)
				metadataJob.setFailureReason(errorMsg);
		} catch (Exception ex) {
			logger.error("Error in SocialWorkerMetadataProcessor:::::saveMetadataJob() -> " + ex.getMessage());
			ex.printStackTrace();
		}
		return metadataJob;
	} */
}
