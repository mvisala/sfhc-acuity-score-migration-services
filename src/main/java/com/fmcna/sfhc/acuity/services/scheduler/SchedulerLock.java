package com.fmcna.sfhc.acuity.services.scheduler;

import java.io.File;
import java.security.SecureRandom;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class SchedulerLock {

	private static final Logger logger = LoggerFactory.getLogger(SchedulerLock.class);

	@Value("${scheduler.lock.timeout}")
	private int lockTimeOutInMinutes;

	@Value("${scheduler.lock.folder}")
	private String lockFolderName;

	@Around("execution(@org.springframework.scheduling.annotation.Scheduled * *(..))")
	public Object lockTask(ProceedingJoinPoint joinPoint) throws Throwable {

		Signature jobSignature = joinPoint.getSignature();
		logger.info("lockTask started !! "  + jobSignature.getDeclaringTypeName());
		StringBuffer fileNameBuffer = new StringBuffer().append(jobSignature.getDeclaringTypeName()).append("_")
				.append(jobSignature.getName()).append(".lck");
		String filePath = fileNameBuffer.toString();
		
		SecureRandom number = SecureRandom.getInstance("SHA1PRNG");

		Object proceed = null;
		File file = null;
		long lockOutTime = System.currentTimeMillis() - (lockTimeOutInMinutes * 60000);
		
		try {
			File directory = new File(lockFolderName);
			
			long randomNumber = number.nextInt(60) + 1;
			logger.info("Thread going to sleep for " + randomNumber + " seconds");
			Thread.sleep(((long)(randomNumber)));
			
			//Sleep random number of seconds
			//double randomNumber1 = Math.floor(Math.random() * 20000) + 1;
	        //logger.info("Thread sleep test ---> " + randomNumber1/1000 + " seconds");
	        
			if (!directory.exists() || !directory.isDirectory()) {
				logger.info("Creating directory : {}", lockFolderName);
				boolean isDirectoryCreated = false;
				isDirectoryCreated = directory.mkdirs();
				if (isDirectoryCreated == true) {
					logger.info("Created directory : {} ", lockFolderName);
				} else {
					logger.info("Unable to create directory {} ", lockFolderName);
				}
			}

			Boolean createNewFile = false;
			StringBuffer filePathBuffer = new StringBuffer().append(lockFolderName).append(File.separator)
					.append(fileNameBuffer);
			file = new File(filePathBuffer.toString());

			if (file.exists()) {
				logger.info("Lock File exists with last modified time : {} ", file.lastModified());
				if (file.lastModified() < lockOutTime) {
					boolean isDeleted = file.delete();

					if (isDeleted == true) {
						logger.warn("Deleted stale lock: {}", filePath);
						createNewFile = true;
					} else {
						logger.error("Unable to delete stale lock...");
					}
				} else { //file.lastModified() >= lockOutTime
					logger.warn("Job is currently locked: {}", filePath);
					return null;
				}
			} else {
				createNewFile = true;
			}	

			if (createNewFile) {
				boolean isCreated = file.createNewFile();
				if (isCreated == true) {
					logger.info("New Lock File created with signature : {}", filePath);
				} else { 
					logger.warn("Named file already exists...");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.warn("Job is currently locked: {}", filePath);
			return null;
		}

		try {
			proceed = joinPoint.proceed();
		} catch (Exception e) {
			logger.error("Error in execution of the scheduled job {}", filePath, e);
		} finally {

			if (file != null && file.exists()) {
				logger.info("Scheduler Lock File exists...");
			    Thread.sleep(((long)(60000)));
			    logger.info("Going to delete scheduler lock...");
				boolean isDeleted = file.delete();
				if (isDeleted == true) {
					logger.info("Deleted from scheduler lock with signature : {}", filePath);
				} else {
					logger.error("UNABLE to delete scheduler lock file...");
				}
			}
		}

		return proceed;
	}
}
