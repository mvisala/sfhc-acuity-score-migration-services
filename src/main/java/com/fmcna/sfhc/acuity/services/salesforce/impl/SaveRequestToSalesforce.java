package com.fmcna.sfhc.acuity.services.salesforce.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fmcna.sfhc.acuity.beans.DieticianMetadata;
import com.fmcna.sfhc.acuity.beans.SocialWorkerMetadata;
import com.fmcna.sfhc.acuity.beans.enums.ResourceJobStatusEnum;
import com.fmcna.sfhc.acuity.configuration.SalesforceConnection;
import com.fmcna.sfhc.acuity.constants.AcuityConstants;
import com.fmcna.sfhc.acuity.persistence.jobs.beans.AcuityScoreMetadataJobStatus;
import com.fmcna.sfhc.acuity.services.RepositoryService;
import com.fmcna.sfhc.acuity.services.exception.SalesforceConnectionException;
import com.fmcna.sfhc.acuity.services.salesforce.ISaveRequestToSF;
import com.fmcna.sfhc.acuity.util.AcuityUtil;
import com.sforce.soap.partner.UpsertResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;

@Component
public class SaveRequestToSalesforce implements ISaveRequestToSF {
	private static Logger logger = LoggerFactory.getLogger(SaveRequestToSalesforce.class);

	@Value("${max.records.sentto.salesforce}")
	private int salesforceMaxLimit;

	@Autowired
	private SalesforceConnection salesforceConnection;
	
	@Autowired
	private ObjectMapper jsonMapper;
	
	@Autowired
	private RepositoryService repositoryService;
	
	@Override
	public UpsertResult[] saveSocialWorkerAcuityScoresToSf(List<AcuityScoreMetadataJobStatus> socialWorkerMetadataList) {
		int i = 1;
		Map<Integer, Long> patients = this.getPatients();
		
		List<SObject> socialworkerSfObjectList = new ArrayList<SObject>();
		
		for (AcuityScoreMetadataJobStatus metadata : socialWorkerMetadataList) {
			SocialWorkerMetadata metadataValues = null;;
			try {
				if (metadata != null) {
					metadataValues = jsonMapper.readValue(AcuityUtil.getClobAsString(metadata.getRequestJson()), SocialWorkerMetadata.class);
					if (metadataValues.getMrn() == null) {
						logger.error("Unable to process score to Salesforce, Unabel to fetch MRN from json");
						repositoryService.updateAcuityScoreMetadataJobStatusBySfFailureStatus(ResourceJobStatusEnum.SF_FAILED.getStatus(), "Unabel to fetch MRN from json", new Date(), metadata.getJobId());
					} else if (metadataValues.getSwAcuityGroup() == null){
						logger.error("Unable to process score to Salesforce, Unable to fetch SwAcuityGroup from json");
						repositoryService.updateAcuityScoreMetadataJobStatusBySfFailureStatus(ResourceJobStatusEnum.SF_FAILED.getStatus(), "Unable to fetch SwAcuityGroup from json", new Date(), metadata.getJobId());
					} else {
						SObject sfObject = new SObject();
						sfObject.setType(AcuityConstants.SF_HEALTHCLOUDGA__EhrPATIENT);
						sfObject.setField("Acuity_Level_SW__c", metadataValues.getSwAcuityGroup());
						sfObject.setField("Source_System_Id_Unique__c", Long.toString(metadataValues.getMrn()));
						
						//sfObject.setField("Acuity_Level_SW__c", metadataValues.getSwAcuityGroup()); //DEV
						//sfObject.setField("Source_System_Id_Unique__c", "5000306804");
						//sfObject.setField("Source_System_Id_Unique__c", Long.toString(patients.get(i++))); 
						
						/*if (i ++ % 2 == 0)
							sfObject.setField("Acuity_Level_SW__c", "Low");
						else 
							sfObject.setField("Acuity_Level_SW__c", "High"); 
						 */
						
						socialworkerSfObjectList.add(sfObject);
					}
				} else {
					logger.error("Metadata is null and unable to map metadata json to Social Worker Metadata");
				}
			} catch (JsonParseException ex) {
				logger.error("JsonParseException in SaveRequestToSalesforce:::saveSocialWorkerAcuityScoresToSf() ==>>> " + ex.getMessage());
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				logger.error("JsonMappingException in SaveRequestToSalesforce:::saveSocialWorkerAcuityScoresToSf() ==>>> " + ex.getMessage());
				ex.printStackTrace();
			} catch (IOException ex) {
				logger.error("IOError in SaveRequestToSalesforce:::saveSocialWorkerAcuityScoresToSf() ==>>> " + ex.getMessage());
				ex.printStackTrace();
			} catch (Exception ex) {
				logger.error("Error in SaveRequestToSalesforce:::saveSocialWorkerAcuityScoresToSf() ==>>> " + ex.getMessage());
				ex.printStackTrace();
			}
		}
		
		return this.salesforceUpsertRecords("Source_System_Id_Unique__c", socialworkerSfObjectList);
	}
	
	@Override
	public UpsertResult[] saveDieticianAcuityScoresToSf(List<AcuityScoreMetadataJobStatus> dieticianMetadataList) {
		
		List<SObject> socialworkerSfObjectList = new ArrayList<SObject>();
		for (AcuityScoreMetadataJobStatus metadata : dieticianMetadataList) {
			try {
				if (metadata != null) {
					DieticianMetadata metadataValues = jsonMapper.readValue(AcuityUtil.getClobAsString(metadata.getRequestJson()), DieticianMetadata.class);
					if (metadataValues.getMrn() == null) {
						logger.error("Unable to process score to Salesforce, MRN is null");
						repositoryService.updateAcuityScoreMetadataJobStatusBySfFailureStatus(ResourceJobStatusEnum.SF_FAILED.getStatus(), "Unabel to fetch MRN from json", new Date(), metadata.getJobId());
					} else if (metadataValues.getTotalRdAcuityScore() == null){
						logger.error("Unable to process score to Salesforce, TotalRdAcuityScore is null");
						repositoryService.updateAcuityScoreMetadataJobStatusBySfFailureStatus(ResourceJobStatusEnum.SF_FAILED.getStatus(), "Unable to fetch TotalRdAcuityScore from json", new Date(), metadata.getJobId());
					} else {
						SObject sfObject = new SObject();
						sfObject.setType(AcuityConstants.SF_HEALTHCLOUDGA__EhrPATIENT);
						sfObject.setField("Acuity_Index_RD__c", Integer.toString(metadataValues.getTotalRdAcuityScore()));
						sfObject.setField("Source_System_Id_Unique__c", Long.toString(metadataValues.getMrn()));  //5000602122
						socialworkerSfObjectList.add(sfObject);
					}
				} else {
					logger.error("JSON Metadata is null and unable to map metadata json to Dietician Metadata");
				}
			} catch (JsonParseException ex) {
				logger.error("JsonParseException in SaveRequestToSalesforce:::saveDieticianAcuityScoresToSf() ==>>> " + ex.getMessage());
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				logger.error("JsonMappingException in SaveRequestToSalesforce:::saveDieticianAcuityScoresToSf() ==>>> " + ex.getMessage());
				ex.printStackTrace();
			} catch (IOException ex) {
				logger.error("IOError in SaveRequestToSalesforce:::saveDieticianAcuityScoresToSf() ==>>> " + ex.getMessage());
				ex.printStackTrace();
			} catch (Exception ex) {
				logger.error("Error in SaveRequestToSalesforce:::saveDieticianAcuityScoresToSf() ==>>> " + ex.getMessage());
				ex.printStackTrace();
			}
		}

		return this.salesforceUpsertRecords("Source_System_Id_Unique__c", socialworkerSfObjectList);
	}
	
	//Soap API requests to Salesforce
	private UpsertResult[] salesforceUpsertRecords(String externalIDFieldName, List<SObject> sfObjectList) {

		int i = 0;
		UpsertResult[] results = {};
		SObject[] salesforceRequests = new SObject[sfObjectList.size()];

		for (SObject s : sfObjectList) {
			salesforceRequests[i] = s;
			i++;
		}
		logger.info("Sending No of request to Salesforce for upsert :" + salesforceRequests.length);

		if (salesforceRequests.length > 0) {
			try {
				results = salesforceConnection.getPartnerConnection().upsert(externalIDFieldName, salesforceRequests);
			} catch (ConnectionException ex) {
				logger.error(ex.getMessage());
				ex.printStackTrace();
			} catch (SalesforceConnectionException ex) {
				logger.error(ex.getMessage());
				ex.printStackTrace();
			} catch (Exception ex) {
				logger.error("Error in SaveRequestToSalesforce:::salesforceUpsertRecords() ==>>> " + ex.getMessage());
				ex.printStackTrace();
			}
		}

		return results;
	}
	
	private Map<Integer, Long> getPatients() {
		Map<Integer, Long> patients = new HashMap<>();
		patients.put(1,1234567L);
		patients.put(2,2000022265L);
		patients.put(3,2000020742L);
		patients.put(4,1000005834L);
		patients.put(5,1000005350L);
		patients.put(6,1000716220L);
		patients.put(7,1000751642L);
		patients.put(8,1000129897L);
		patients.put(9,1000746452L);
		patients.put(10,1000153891L);
		patients.put(11,1000155522L);
		patients.put(12,1000153023L);
		patients.put(13,1000004003L);
		patients.put(14,1000004908L);
		patients.put(15,1000153864L);
		patients.put(16,1000153009L);
		patients.put(17,1000173532L);
		patients.put(18,1000173196L);
		patients.put(19,1000153049L);
		patients.put(20,1000174068L);
		patients.put(21,1000203968L);
		patients.put(22,64550L);
		patients.put(23,1000173616L);
		patients.put(24,1000171397L);
		patients.put(25,1000171399L);
		patients.put(26,1000173564L);
		patients.put(27,1000717626L);
		patients.put(28,1000727141L);
		patients.put(29,1000727207L);
		patients.put(30,1000723186L);
		patients.put(31,2000023442L);
		patients.put(32,2000022257L);
		patients.put(33,2184L);
		patients.put(34,1000004362L);
		patients.put(35,1000004077L);
		patients.put(36,1000130124L);
		patients.put(37,1000132181L);
		patients.put(38,1000134509L);
		patients.put(39,1000135696L);
		patients.put(40,1000719205L);
		patients.put(41,103800L);
		patients.put(42,218768L);
		patients.put(43,1000717728L);
		patients.put(44,1000712800L);
		patients.put(45,1000714027L);
		patients.put(46,1000723167L);
		patients.put(47,1000722633L);
		patients.put(48,1000713820L);
		patients.put(49,2000023446L);
		patients.put(50,2000021934L);
		patients.put(51,1000005529L);
		patients.put(52,1000718904L);
		patients.put(53,1000715669L);
		patients.put(54,1000155702L);
		patients.put(55,1000173768L);
		patients.put(56,123973L);
		patients.put(57,1000134503L);
		patients.put(58,1000131276L);
		patients.put(59,1000716229L);
		patients.put(60,1000716359L);
		patients.put(61,1000715389L);
		patients.put(62,1000155661L);
		patients.put(63,1000173629L);
		patients.put(64,1000726763L);
		patients.put(65,1000173677L);
		patients.put(66,1000172787L);
		patients.put(67,1000173102L);
		patients.put(68,1000131161L);
		patients.put(69,1000763293L);
		patients.put(70,1000154586L);
		patients.put(71,1000155105L);
		patients.put(72,1000153919L);
		patients.put(73,1000173811L);
		patients.put(74,5000306482L);
		patients.put(75,5000602203L);
		patients.put(76,5000305980L);
		patients.put(77,5000307476L);
		patients.put(78,5000306546L);
		patients.put(79,5000307530L);
		patients.put(80,5000306586L);
		patients.put(81,5000306804L);
		patients.put(82,5000601994L);
		patients.put(83,5000601991L);
		patients.put(84,5000601992L);
		patients.put(85,5000601997L);
		patients.put(86,5000602000L);
		patients.put(87,5000601999L);
		patients.put(88,5000602001L);
		patients.put(89,5000601998L);
		patients.put(90,5000601924L);
		patients.put(91,5000601944L);
		patients.put(92,1000135257L);
		patients.put(93,1000119398L);
		patients.put(94,1000219772L);
		patients.put(95,1000002121L);
		patients.put(96,1000002297L);
		patients.put(97,1000152527L);
		patients.put(98,1000728277L);
		patients.put(99,1000653612L);
		patients.put(100,1000007296L);
		patients.put(101,1000173692L);
		patients.put(102,2000006654L);
		patients.put(103,1000164638L);
		patients.put(104,1000777207L);
		patients.put(105,2000007182L);
		patients.put(106,212231L);
		patients.put(107,1000172862L);
		patients.put(108,212930L);
		patients.put(109,1000165558L);
		patients.put(110,1000161326L);
		patients.put(111,206926L);
		patients.put(112,1000002287L);
		patients.put(113,1000725891L);
		patients.put(114,217404L);
		patients.put(115,2000023632L);
		patients.put(116,1000153937L);
		patients.put(117,2000021488L);
		patients.put(118,1000117001L);
		patients.put(119,1000111063L);
		patients.put(120,1000065479L);
		patients.put(121,2000014565L);
		patients.put(122,1000217192L);
		patients.put(123,2000023383L);
		patients.put(124,2000005183L);
		patients.put(125,1000798062L);
		patients.put(126,1000729097L);
		patients.put(127,1000725902L);
		patients.put(128,1000767397L);
		patients.put(129,1000728296L);
		patients.put(130,1000178437L);
		patients.put(131,1000161012L);
		patients.put(132,1000094792L);
		patients.put(133,1000172641L);
		patients.put(134,1000161702L);
		patients.put(135,2000013717L);
		patients.put(136,1000654806L);
		patients.put(137,1000005291L);
		patients.put(138,1000097649L);
		patients.put(139,1000209961L);
		patients.put(140,1000173103L);
		patients.put(141,199227L);
		patients.put(142,1000151966L);
		patients.put(143,1000005353L);
		patients.put(144,1000132501L);
		patients.put(145,1000218717L);
		patients.put(146,1000173356L);
		patients.put(147,1000164667L);
		patients.put(148,1000111574L);
		patients.put(149,2000021124L);
		patients.put(150,207313L);
		patients.put(151,1000209391L);
		patients.put(152,199221L);
		patients.put(153,1000022051L);
		patients.put(154,1000004422L);
		patients.put(155,100837L);
		patients.put(156,218888L);
		patients.put(157,2000023050L);
		patients.put(158,1000216677L);
		patients.put(159,171584L);
		patients.put(160,1000209955L);
		patients.put(161,1000728086L);
		patients.put(162,1000132218L);
		patients.put(163,1000172917L);
		patients.put(164,1000107607L);
		patients.put(165,1000726448L);
		patients.put(166,1000781878L);
		patients.put(167,1000103928L);
		patients.put(168,1000177804L);
		patients.put(169,1000158850L);
		patients.put(170,1000760336L);
		patients.put(171,1000136480L);
		patients.put(172,2000013215L);
		patients.put(173,1000020552L);
		patients.put(174,1000016383L);
		patients.put(175,2000007531L);
		patients.put(176,1000718453L);
		patients.put(177,1000178598L);
		patients.put(178,1000711687L);
		patients.put(179,1000164270L);
		patients.put(180,1000069944L);
		patients.put(181,1000161455L);
		patients.put(182,1000757032L);
		patients.put(183,1000007240L);
		patients.put(184,1000018727L);
		patients.put(185,1000020269L);
		patients.put(186,2000022518L);
		patients.put(187,1000727983L);
		patients.put(188,1000764563L);
		patients.put(189,1000717350L);
		patients.put(190,2000022294L);
		patients.put(191,2000020249L);
		patients.put(192,2000020987L);
		patients.put(193,1000724687L);
		patients.put(194,1000129919L);
		patients.put(195,1000750180L);
		patients.put(196,1000119455L);
		patients.put(197,1000210804L);
		patients.put(198,2000020407L);
		patients.put(199,3000085674L);
		patients.put(200,1000005465L);
		
		return patients;
	}
}
