package com.fmcna.sfhc.acuity.services.mapper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.sql.rowset.serial.SerialClob;

import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.dstu3.model.Bundle.BundleEntryRequestComponent;
import org.hl7.fhir.dstu3.model.Bundle.HTTPVerb;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Condition;
import org.hl7.fhir.dstu3.model.Condition.ConditionEvidenceComponent;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.MeasureReport;
import org.hl7.fhir.dstu3.model.MeasureReport.MeasureReportGroupComponent;
import org.hl7.fhir.dstu3.model.Meta;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.dstu3.model.StringType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fmcna.sfhc.acuity.beans.DieticianMetadata;
import com.fmcna.sfhc.acuity.beans.SocialWorkerMetadata;
import com.fmcna.sfhc.acuity.beans.enums.ResourceJobStatusEnum;
import com.fmcna.sfhc.acuity.constants.AcuityConstants;
import com.fmcna.sfhc.acuity.fhir.KafkaProducer;
import com.fmcna.sfhc.acuity.persistence.jobs.beans.AcuityScoreMetadataJobStatus;
import com.fmcna.sfhc.acuity.services.RepositoryService;
import com.fmcna.sfhc.acuity.util.FhirUtil;

import ca.uhn.fhir.context.FhirContext;

@Service
public class FHIRRequestMapper {
	private static final Logger logger = LoggerFactory.getLogger(FHIRRequestMapper.class);

	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private FhirContext fhirContext;
	
	@Autowired
	private KafkaProducer kafkaProducer;
	
	@Autowired
	private RepositoryService repositoryService;

	/**
	 * To create Bundle - Measure Report
	 * @param list of metadata
	 * @return
	 */
	
	public String createSocialWorkerBundleMessage(Long acuityId, List<SocialWorkerMetadata> metadataSubList) {
		Bundle bundle = this.createFhirBundleResource();
		int count = 1;
		String measureRptBundle = null;
		MeasureReport measureRpt = null;
		try {
			List<AcuityScoreMetadataJobStatus> jobSubList = new ArrayList<>();
			for (SocialWorkerMetadata metadata :metadataSubList) {
				//if (count == 1)
				//	metadata.setMrn(Long.parseLong("5000448375"));
				//Thread.sleep(1000);
				
				if (metadata.getSwAcuityGroup() != null) {
					measureRpt = this.createSwMeasureReport(metadata);
					
					if (measureRpt.getId() != null) {
						jobSubList.add(this.getSwMetadataJob(acuityId, metadata, measureRpt.getId(), ResourceJobStatusEnum.NEW.name(), null));
					} else {
						repositoryService.saveAcuityScoreMetadata(this.getSwMetadataJob(acuityId, metadata, measureRpt.getId(), ResourceJobStatusEnum.FAILED.name(), "Unable to generate Fhir_Id"));
					}
					
					if (count == 1) {
						//bundle.setId(measureRpt.getId());
						bundle.setId(FhirUtil.generateBundleId(metadata.getMrn().toString(), metadata.getPredictionDate()));
						count++;
					}
					
					BundleEntryComponent bc1 = new BundleEntryComponent();
					bc1.setFullUrl("MeasureReport/" + measureRpt.getId());
					bc1.setResource(measureRpt);
		
					BundleEntryRequestComponent br1 = new BundleEntryRequestComponent();
					br1.setMethod(HTTPVerb.PUT);
					br1.setUrl("MeasureReport/" + measureRpt.getId());
					bc1.setRequest(br1);
					bundle.addEntry(bc1);
				} else {
					repositoryService.saveAcuityScoreMetadata(this.getSwMetadataJob(acuityId, metadata, measureRpt.getId(), ResourceJobStatusEnum.FAILED.name(), "SwAcuityGroup is null"));
				}
			}
		
			jobSubList = this.saveBulkMetadataList(jobSubList);
			
			if (jobSubList.size() > 0) {
				logger.info("Successfully persisted {} sw jobs, going to create bundle message and push to Kafka Topic..... ", jobSubList.size());
				measureRptBundle = fhirContext.newJsonParser().encodeResourceToString(bundle);
				
				if (measureRptBundle != null) {
					this.sendBundleToFhir(jobSubList, measureRptBundle);
				} else {
					logger.error("Unable to create bundle message ");
				}
			} else {
				logger.error("Unable to persist sw jobs  ", jobSubList.size());
			}
			
		} catch (Exception ex) {
			logger.error("Error in FHIRRequestMapper:::createSocialWorkerBundleMessage() -> " + ex.getMessage());
			ex.printStackTrace();
		}

		return measureRptBundle;
	}
	
	private void sendBundleToFhir(List<AcuityScoreMetadataJobStatus> jobSubList, String fhirBundleMsg) {
		List<Long> jobIds = jobSubList.stream().map(AcuityScoreMetadataJobStatus::getJobId).collect(Collectors.toList());
		int jobListSize = jobIds.size();
		int updateCount = 0;
		
		if (kafkaProducer.sendMeasureReportBundle(fhirBundleMsg)) {
			if (jobListSize > 0) {
				updateCount = repositoryService.updateAcuityScoreMetadataJobStatusByJobStatus(ResourceJobStatusEnum.FHIR_SENT.name(), new Date(), jobIds);
				logger.info("Successfully processed {} records to FHIR, updated status to {} ", updateCount, ResourceJobStatusEnum.FHIR_SENT.name()); 
			}
		} else {
			logger.error("Unable to push bundle to Kafka Topic... ");
			if (jobListSize > 0) {
				updateCount = repositoryService.updateAcuityScoreMetadataJobStatusByJobStatus(ResourceJobStatusEnum.FHIR_FAILED.name(), new Date(), jobIds);
				logger.info("Unable to push bundle to Topic, updated {} records with {} status", updateCount, ResourceJobStatusEnum.FHIR_FAILED.name());
			}
		}
	}

	public String createDieticianBundleMessage(Long acuityId, List<DieticianMetadata> metadataSubList) {
		Bundle bundle = this.createFhirBundleResource();
		int count = 1;
		String measureRptBundle = null;
		MeasureReport measureRpt = null;
		try {
			List<AcuityScoreMetadataJobStatus> jobSubList = new ArrayList<>();
			for (DieticianMetadata metadata :metadataSubList) {
				//if (count == 1)
				//	metadata.setMrn(Long.parseLong("5000448375"));
				//Thread.sleep(1000);
				
				if (metadata.getTotalRdAcuityScore() != null) {
					measureRpt = this.createRdMeasureReport(metadata);
					
					if (measureRpt.getId() != null) {
						jobSubList.add(this.getRdMetadataJob(acuityId, metadata, measureRpt.getId(), ResourceJobStatusEnum.NEW.name(), null));
					} else {
						repositoryService.saveAcuityScoreMetadata(this.getRdMetadataJob(acuityId, metadata, measureRpt.getId(), ResourceJobStatusEnum.FHIR_FAILED.name(), "Unable to generate Fhir_Id"));
					}
					
					if (count == 1) {
						//bundle.setId(measureRpt.getId());
						bundle.setId(FhirUtil.generateBundleId(metadata.getMrn().toString(), metadata.getPredictionDate()));
						count++;
					}
					
					BundleEntryComponent bc1 = new BundleEntryComponent();
					bc1.setFullUrl("MeasureReport/" + measureRpt.getId());
					bc1.setResource(measureRpt);
		
					BundleEntryRequestComponent br1 = new BundleEntryRequestComponent();
					br1.setMethod(HTTPVerb.PUT);
					br1.setUrl("MeasureReport/" + measureRpt.getId());
					bc1.setRequest(br1);
					bundle.addEntry(bc1);
				} else {
					repositoryService.saveAcuityScoreMetadata(this.getRdMetadataJob(acuityId, metadata, measureRpt.getId(), ResourceJobStatusEnum.FHIR_FAILED.name(), "TotalRdAcuityScore is null"));
				}
			}
		
			jobSubList = this.saveBulkMetadataList(jobSubList);
			
			if (jobSubList.size() > 0) {
				logger.info("Successfully persisted {} rd jobs, going to create bundle message and push to Kafka Topic..... ", jobSubList.size());
				measureRptBundle = fhirContext.newJsonParser().encodeResourceToString(bundle);
				
				if (measureRptBundle != null) {
					this.sendBundleToFhir(jobSubList, measureRptBundle);
				}
			} else {
				logger.error("Unable to persist rd jobs...");
			}
		} catch (Exception ex) {
			logger.error("Error in FHIRRequestMapper:::createDieticianBundleMessage() -> " + ex.getMessage());
			ex.printStackTrace();
		}

		return measureRptBundle;
	}
	
	private List<AcuityScoreMetadataJobStatus> saveBulkMetadataList(List<AcuityScoreMetadataJobStatus> jobSubList) {
		try {
			jobSubList = repositoryService.saveAcuityScoreMetadataList(jobSubList);
		} catch (Exception ex) {
			logger.error("Error in FHIRRequestMapper:::saveBulkMetadataList() -> " + ex.getMessage());
			ex.printStackTrace();
		}
		
		return jobSubList;
	}
	
	private AcuityScoreMetadataJobStatus getSwMetadataJob(Long acuityId, SocialWorkerMetadata metadata, String fhirId, String jobStatus, String errorMsg) {
		AcuityScoreMetadataJobStatus metadataJob = null;
		try {
			metadataJob = new AcuityScoreMetadataJobStatus();
			metadataJob.setAcuityId(acuityId);
			metadataJob.setJobType("SW_JOB");
			metadataJob.setCreatedDate(new Date());
			metadataJob.setMrn(metadata.getMrn());
			metadataJob.setPredictionDate(metadata.getPredictionDate());
			//metadataJob.setMrn(5000448375L); //DEV-FHIR
			
			try {
				metadataJob.setRequestJson(new SerialClob(objectMapper.writeValueAsString(metadata).toString().toCharArray()));
			} catch (JsonProcessingException | SQLException e) {
				jobStatus = ResourceJobStatusEnum.FHIR_FAILED.name();
				errorMsg = e.getMessage();
				e.printStackTrace();
			}
			metadataJob.setFhirId(fhirId);
			metadataJob.setJobStatus(jobStatus);
			
			if (errorMsg != null)
				metadataJob.setFailureReason(errorMsg);
		} catch (Exception ex) {
			logger.error("Error in FHIRRequestMapper:::getSwMetadataJob() -> " + ex.getMessage());
			ex.printStackTrace();
		}
		return metadataJob;
	}
	
	private AcuityScoreMetadataJobStatus getRdMetadataJob(Long acuityId, DieticianMetadata metadata, String fhirId, String jobStatus, String errorMsg) {
		AcuityScoreMetadataJobStatus metadataJob = null;
		try {
			metadataJob = new AcuityScoreMetadataJobStatus();
			metadataJob.setAcuityId(acuityId);
			metadataJob.setJobType("RD_JOB");
			metadataJob.setCreatedDate(new Date());
			metadataJob.setMrn(metadata.getMrn());
			metadataJob.setPredictionDate(metadata.getPredictionDate());
			//metadataJob.setMrn(5000448375L); //DEV-FHIR
			try {
				metadataJob.setRequestJson(new SerialClob(objectMapper.writeValueAsString(metadata).toString().toCharArray()));
			} catch (JsonProcessingException | SQLException e) {
				jobStatus = ResourceJobStatusEnum.FHIR_FAILED.name();
				errorMsg = e.getMessage();
				e.printStackTrace();
			}
			metadataJob.setFhirId(fhirId);
			metadataJob.setJobStatus(jobStatus);
			
			if (errorMsg != null)
				metadataJob.setFailureReason(errorMsg);
		} catch (Exception ex) {
			logger.error("Error in FHIRRequestMapper:::getRdMetadataJob() -> " + ex.getMessage());
			ex.printStackTrace();
		}
		return metadataJob;
	}
	
	/*public String createSocialWorkerBundleMessage(List<SocialWorkerMetadata> metadataSubList) {
		Bundle bundle = this.createFhirBundleResource();
		int count = 1;
		String measureRptBundle = null;
		MeasureReport measureRpt = null;
		
		try {
			for (SocialWorkerMetadata metadata :metadataSubList) {
				
				//if (count == 1)
				//	metadata.setMrn(Long.parseLong("5000448375"));
				
				//Thread.sleep(1000);
				measureRpt = this.createSwMeasureReport(metadata);
				
				if (measureRpt.getId() != null) {
					repositoryService.updateAcuityScoreMetadataByFhirId(ResourceJobStatusEnum.FHIR_SENT.getStatus(),
							measureRpt.getId(), new Date(), metadata.getMrn(), metadata.getId());
				}
				
				if (count == 1) {
					//bundle.setId(measureRpt.getId());
					bundle.setId(FhirUtil.generateBundleId(metadata.getMrn().toString(), metadata.getPredictionDate()));
					count++;
				}
				
				//String measureReport = fhirContext.newJsonParser().encodeResourceToString(measureRpt);
				//System.out.println(measureReport);
				
				BundleEntryComponent bc1 = new BundleEntryComponent();
				bc1.setFullUrl("MeasureReport/" + measureRpt.getId());
				bc1.setResource(measureRpt);
	
				BundleEntryRequestComponent br1 = new BundleEntryRequestComponent();
				br1.setMethod(HTTPVerb.PUT);
				br1.setUrl("MeasureReport/" + measureRpt.getId());
				bc1.setRequest(br1);
				bundle.addEntry(bc1);
			}
		
			measureRptBundle = fhirContext.newJsonParser().encodeResourceToString(bundle);
			
		} catch (Exception ex) {
			logger.error("Error in createSocialWorkerBundleMessage() -> " + ex.getMessage());
			ex.printStackTrace();
		}

		return measureRptBundle;
	} 
	
	public String createDieticianBundleMessage(List<DieticianMetadata> metadataSubList) {
		Bundle bundle = this.createFhirBundleResource();
		int count = 1;
		String measureRptBundle = null;
		MeasureReport measureRpt = null;
		
		try {
			
			for (DieticianMetadata metadata :metadataSubList) {
				
				//if (count == 1)
				//	metadata.setMrn(Long.parseLong("5000448375"));
				
				//Thread.sleep(1000);
				measureRpt = this.createRdMeasureReport(metadata);
				
				if (measureRpt.getId() != null) {
					//logger.info("Successfully created Measure Report with Id ::: " + measureRpt.getId());
					repositoryService.updateAcuityScoreMetadataByFhirId(ResourceJobStatusEnum.FHIR_SENT.getStatus(),
							measureRpt.getId(), new Date(), metadata.getMrn(), metadata.getId());
				}
				
				if (count == 1) {
					//bundle.setId(measureRpt.getId());
					bundle.setId(FhirUtil.generateBundleId(metadata.getMrn().toString(), metadata.getPredictionDate()));
					count++;
				}
				
				BundleEntryComponent bc1 = new BundleEntryComponent();
				bc1.setFullUrl("MeasureReport/" + measureRpt.getId());
				bc1.setResource(measureRpt);
	
				BundleEntryRequestComponent br1 = new BundleEntryRequestComponent();
				br1.setMethod(HTTPVerb.PUT);
				br1.setUrl("MeasureReport/" + measureRpt.getId());
				bc1.setRequest(br1);
				bundle.addEntry(bc1);
			}
		
			measureRptBundle = fhirContext.newJsonParser().encodeResourceToString(bundle);
			
		} catch (Exception ex) {
			logger.error("Error in createDieticianBundleMessage() -> " + ex.getMessage());
			ex.printStackTrace();
		}

		return measureRptBundle;
	}
	
	*/
	
	private Bundle createFhirBundleResource() {
		Bundle bundle = new Bundle();
		List<Coding> tagList = new ArrayList<>();
		
		Coding coding = new Coding();
		coding.setSystem(AcuityConstants.ACUITY_BUNDLE_IDENTIFIER_SYSTEM_URN);
		coding.setCode(AcuityConstants.BUNDLE_ACUITY_CODE);
		tagList.add(coding);

		Meta meta = new Meta();
		meta.setTag(tagList);

		bundle.setMeta(meta);
		//bundle.setType(Bundle.BundleType.TRANSACTION);
		bundle.setType(Bundle.BundleType.BATCH);

		return bundle;
	}

	private MeasureReport createSwMeasureReport(SocialWorkerMetadata metadata) {
		MeasureReport report = null;
		try {
			report = new MeasureReport();
			//List<Resource> resourceList = createBundleResourceList(metadata);
			//report.setContained(resourceList);
			List<Extension> extensionList = new ArrayList<>();
			Extension extension = new Extension();
			extension.setUrl("KCNG#SW_ACUITY_GROUP").setValue(new StringType(metadata.getSwAcuityGroup()));
			
			Identifier identifier = new Identifier();
			identifier.setSystem(AcuityConstants.MEASURE_REPORT_IDENTIFIER_SYSTEM_URN);
			//identifier.setValue("5000602451SW");
			
			String refId = FhirUtil.generateReferenceId(metadata.getMrn().toString(), AcuityConstants.FMC_ACUITY_TYPE_SW_RESOURCE);
			identifier.setValue(refId);
			
			report.setIdentifier(identifier);
			report.setId("MRPT" +refId);
			
			extensionList.add(extension);
			report.setExtension(extensionList);
			
			report.setPatient(new Reference("Patient/P" + metadata.getMrn()));
			report.setStatus(MeasureReport.MeasureReportStatus.COMPLETE);
			report.setType(MeasureReport.MeasureReportType.INDIVIDUAL);
			report.setMeasure(new Reference("Measure/MEASUREACUITYSW"));
			
			Period period = new Period();
			period.setStart(metadata.getPredictionDate()); //new Date()
			report.setPeriod(period);
			
			report.setDate(metadata.getPredictionDate()); //new Date()
			
			List<MeasureReportGroupComponent> measureRptGroup = new ArrayList<>();
			
			measureRptGroup.add(new MeasureReportGroupComponent()
					.setIdentifier(new Identifier().setValue("TOTAL_SW_ACUITY_INDEX_SCORE"))
					.setMeasureScore(metadata.getTotalSwPoints()));
			
			report.setGroup(measureRptGroup);
			//report.setEvaluatedResources(new Reference("#REASONS"));
		} catch (Exception ex) {
			logger.error("Error occured in FHIRRequestMapper:::createSwMeasureReport() -> " + ex.getMessage());
			ex.printStackTrace();
		}
		
		return report;
	}
	
	private MeasureReport createRdMeasureReport(DieticianMetadata metadata) {
		MeasureReport report = null;
		try {
			report = new MeasureReport();
			Identifier identifier = new Identifier();
			identifier.setSystem(AcuityConstants.MEASURE_REPORT_IDENTIFIER_SYSTEM_URN);
			//identifier.setValue("5000602451SW");
			
			String refId = FhirUtil.generateReferenceId(metadata.getMrn().toString(), AcuityConstants.FMC_ACUITY_TYPE_RD_RESOURCE);
			identifier.setValue(refId);
			
			report.setIdentifier(identifier);
			report.setId("MRPT" +refId);
			
			report.setPatient(new Reference("Patient/P" + metadata.getMrn()));
			report.setStatus(MeasureReport.MeasureReportStatus.COMPLETE);
			report.setType(MeasureReport.MeasureReportType.INDIVIDUAL);
			report.setMeasure(new Reference("Measure/MEASUREACUITYRD"));
			
			Period period = new Period();
			period.setStart(metadata.getPredictionDate());
			report.setPeriod(period);
			
			report.setDate(metadata.getPredictionDate());
			
			List<MeasureReportGroupComponent> measureRptGroup = new ArrayList<>();
			
			measureRptGroup.add(new MeasureReportGroupComponent()
					.setIdentifier(new Identifier().setValue("TOTAL_RD_ACUITY_SCORE"))
					.setMeasureScore(metadata.getTotalRdAcuityScore()));
			report.setGroup(measureRptGroup);
		} catch (Exception ex) {
			logger.error("Error occurred in FHIRRequestMapper:::createRdMeasureReport() -> " + ex.getMessage());
			ex.printStackTrace();
		}
		
		return report;
	}
	
	private List<Resource> createBundleResourceList(SocialWorkerMetadata  metadata) {
		String mrn = null;
		Bundle bundle = new Bundle();
		List<Resource> resourceList = new ArrayList<Resource>();
		
		bundle.setId("REASONS");
		mrn = Long.toString(metadata.getMrn());
		
		BundleEntryComponent bc = new BundleEntryComponent();
		//bc.setResource(getEntryCondition(mrn, metadata.getReason1d(), metadata.getReason1Desc(), metadata.getRootCause1()));
		//bc.setResource(getEntryCondition(mrn, metadata.getReason2d(), metadata.getReason1Desc(), metadata.getRootCause1()));
		
		bundle.addEntry(bc);
		resourceList.add(bundle);
		
		return resourceList;
	}
	
	private Condition getEntryCondition(String mrn, String reasonCode, String reasonDescription, String rootCause) {
		
		Condition condition = new Condition();
		List<CodeableConcept> categoryCodeableConcepts = new ArrayList<>();
		
		CodeableConcept concept = FhirUtil.getCodeableConcept("problem-list-item", "Problem List Item", "http://hl7.org/fhir/condition-category");
		categoryCodeableConcepts.add(concept);
		
		condition.setCategory(categoryCodeableConcepts);
		condition.setSubject(new Reference("Patient/P" + mrn));
		
		if (rootCause != null)
			condition.setCode(new CodeableConcept().setText(rootCause));
		
		if (reasonCode != null && reasonDescription != null)
			condition.setEvidence(this.getEvidence(reasonCode, reasonDescription));
		
		return condition;
	}

	private List<ConditionEvidenceComponent> getEvidence(String reasonCode, String reasonDescription) {
		List<ConditionEvidenceComponent> evidenceList = new ArrayList<>();
		
		ConditionEvidenceComponent evidence = new ConditionEvidenceComponent();
		
		List<CodeableConcept> evidenceCodeableConcepts = new ArrayList<>();
		
		CodeableConcept evidenceCode = FhirUtil.getCodeableConcept(reasonCode, null, AcuityConstants.FMC_ACUITY_REASONCODE_SYSTEM_URL);
		evidenceCode.setText(reasonDescription);
		
		evidenceCodeableConcepts.add(evidenceCode);
		evidence.setCode(evidenceCodeableConcepts);
		evidenceList.add(evidence);
		
		return evidenceList;
	}
}
