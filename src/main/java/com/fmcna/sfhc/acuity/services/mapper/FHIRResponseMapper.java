package com.fmcna.sfhc.acuity.services.mapper;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.AuditEvent;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.MeasureReport;
import org.hl7.fhir.dstu3.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Service;

import com.fmcna.sfhc.acuity.beans.enums.ResourceJobStatusEnum;
import com.fmcna.sfhc.acuity.constants.AcuityConstants;
import com.fmcna.sfhc.acuity.services.RepositoryService;
import com.fmcna.sfhc.acuity.services.exception.ServiceException;

import ca.uhn.fhir.context.FhirContext;

@Service
public class FHIRResponseMapper {
	private static Logger logger = LoggerFactory.getLogger(FHIRResponseMapper.class);

	@Autowired
	FhirContext fhirContext;
	
	@Autowired
	RepositoryService repositoryService;

	/**
	 * Process FHIR Measure Report response from topic
	 * @param message
	 * @throws ServiceException 
	 */
	public void processMeasureReport(String message, MessageHeaders headers) {

		try {
			logger.info("Processing Measure Report FHIR Response from Kafka Topic ...");
			AuditEvent auditEvent = (AuditEvent) fhirContext.newJsonParser().parseResource(message);

			if (auditEvent != null && !auditEvent.getContained().isEmpty()) {
				List<Resource> resourceList = auditEvent.getContained();

				for (Resource resource : resourceList) {
					
					if (resource instanceof MeasureReport) {
						MeasureReport measureReport = (MeasureReport) resource;

						if (measureReport.hasIdentifier()) {
							Identifier identifier = measureReport.getIdentifier();

								if (identifier.hasSystem() && AcuityConstants.MEASURE_REPORT_IDENTIFIER_SYSTEM_URN.equals(identifier.getSystem())) {
								logger.info("Message Received from Kafka Topic JPA_MEASUREREPORT ==>>> Offset : {} "
										+ "-> Partition : {} -> FHIR ID : {} -> Prediction Date : {}",
										headers.get("kafka_offset"), headers.get("kafka_receivedPartitionId"),
										measureReport.getId(), measureReport.getDate());

								if (measureReport.getDate() != null) {
									repositoryService.updateAcuityScoreMetadataJobStatusByFhirStatus(
											ResourceJobStatusEnum.FHIR_SUCCESS.getStatus(), new Date(),
											measureReport.getId().replace("#", ""), measureReport.getDate());
								} else {
									repositoryService.updateAcuityScoreMetadataJobStatusByFhirStatus(
											ResourceJobStatusEnum.FAILED.getStatus(), new Date(),
											measureReport.getId().replace("#", ""), measureReport.getDate());
									logger.error("Unable to fetch Prediction_Date for Measure Report Fhir Id : {} ", measureReport.getId());
								}
								}
						} else {
							//logger.info("Received NULL Identifier for measureReport Id : " + measureReport.getId());
						}
					}
				}
			}
		} catch (Exception ex) {
			logger.info("Error occurred in FHIRResponseMapper:::processMeasureReport() -> " + ex.getMessage());
			ex.printStackTrace();
		}
	}
}
