package com.fmcna.sfhc.acuity.services.exception;

public class SalesforceConnectionException extends Exception {

	private static final long serialVersionUID = 425045461007631554L;

	public SalesforceConnectionException(String message) {
		super(message);
	}

	public SalesforceConnectionException(String message, Throwable t) {
		super(message, t);
	}

}
