package com.fmcna.sfhc.acuity.services;

import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fmcna.sfhc.acuity.beans.enums.ResourceJobStatusEnum;
import com.fmcna.sfhc.acuity.persistence.jobs.beans.AcuityScoreMetadataJobStatus;
import com.fmcna.sfhc.acuity.services.salesforce.impl.SaveRequestToSalesforce;
import com.google.common.collect.Lists;
import com.sforce.soap.partner.UpsertResult;

@Service
public class SalesforceService {
	private static Logger logger = LoggerFactory.getLogger(SalesforceService.class);

	@Value("${max.records.sentto.salesforce}")
	private int salesforceMaxLimit;
	
	@Autowired
	private SaveRequestToSalesforce saveRequestToSalesforce;

	@Autowired
	private RepositoryService repositoryService;
	
	public void sendSocialWorkerAcuityScoresToSalesforce(List<AcuityScoreMetadataJobStatus> acuityScoreMetadataList) {
		
		List<List<AcuityScoreMetadataJobStatus>> totalMetadataList = Lists.partition(acuityScoreMetadataList, salesforceMaxLimit);
		
		if (totalMetadataList.size() > 0) {
			Stream<List<AcuityScoreMetadataJobStatus>> parallelStream = totalMetadataList.parallelStream();
			parallelStream.forEach(job -> this.processSWStream(job));
		}
		
		/*for (List<AcuityScoreMetadataJobStatus> metadataSubList :totalMetadataList) {
			UpsertResult[] upsertResults = saveRequestToSalesforce.saveSocialWorkerAcuityScoresToSf(metadataSubList);
			this.processUpsertResults(metadataSubList, upsertResults);
		} */
	}
	 
	public void sendDieticianAcuityScoresToSalesforce(List<AcuityScoreMetadataJobStatus> acuityScoreMetadataList) {
		
		List<List<AcuityScoreMetadataJobStatus>> totalMetadataList = Lists.partition(acuityScoreMetadataList, salesforceMaxLimit);;
		
		if (totalMetadataList.size() > 0) {
			Stream<List<AcuityScoreMetadataJobStatus>> parallelStream = totalMetadataList.parallelStream();
			parallelStream.forEach(job -> this.processRDStream(job));
		}
		/*for (List<AcuityScoreMetadataJobStatus> metadataSubList :totalMetadataList) {
			UpsertResult[] upsertResults = saveRequestToSalesforce.saveDieticianAcuityScoresToSf(metadataSubList);
			this.processUpsertResults(metadataSubList, upsertResults);
		} */
	}
	
	private void processSWStream(List<AcuityScoreMetadataJobStatus> subList) {
		UpsertResult[] upsertResults;
		try {
			logger.info("No. of Social Worker Acuity Scores processed from the metadata list : {}", subList.size());
			
			upsertResults = saveRequestToSalesforce.saveSocialWorkerAcuityScoresToSf(subList);
			this.processUpsertResults(subList, upsertResults);
		} catch (Exception ex) {
			logger.error("Error occurred in SalesforceService:::processSWStream() ==>>> " + ex.getMessage());
			ex.printStackTrace();
		}
	}

	private void processRDStream(List<AcuityScoreMetadataJobStatus> subList) {
		UpsertResult[] upsertResults;
		try {
			logger.info("No. of Dietician Acuity Scores processed from the metadata list : {}", subList.size());
			
			upsertResults = saveRequestToSalesforce.saveDieticianAcuityScoresToSf(subList);
			this.processUpsertResults(subList, upsertResults);
		} catch (Exception ex) {
			logger.error("Error occurred in SalesforceService:::processRDStream() ==>>> " + ex.getMessage());
			ex.printStackTrace();
		}
	}
	
	/*private List<List<AcuityScoreMetadataJobStatus>> updateSalesforceProcessingStatus(List<AcuityScoreMetadataJobStatus> acuityScoreMetadataList) {
		int jobListSize = 0;
		List<List<AcuityScoreMetadataJobStatus>> totalMetadataList = null;
		try {
			int count = 0;
			List<Long> jobIds = acuityScoreMetadataList.stream().map(AcuityScoreMetadataJobStatus::getJobId).collect(Collectors.toList());
			
			jobListSize = jobIds.size();
			
			if (jobListSize > 0) {
				count = repositoryService.updateAcuityScoreMetadataJobStatusByJobStatus(ResourceJobStatusEnum.SF_PROCESSING.name(), new Date(), jobIds);
				
				logger.info("Successfully updated {} records with {} status", count, ResourceJobStatusEnum.SF_PROCESSING.name());
				
				totalMetadataList = Lists.partition(acuityScoreMetadataList, salesforceMaxLimit);
			}
		} catch (Exception ex) {
			logger.error("Error while updating {} status for {} records ", ResourceJobStatusEnum.SF_PROCESSING.name(), jobListSize);
		}
		return totalMetadataList;
	} */

	private void processUpsertResults(List<AcuityScoreMetadataJobStatus> metadataSubList, UpsertResult[] upsertResults) {
		int i = 0;
		String failureMessage = null;
		
		for (UpsertResult s : upsertResults) {
			Long jobId = metadataSubList.get(i).getJobId();
			
			if (s.getSuccess()) {
				String resultId = s.getId();
				logger.info("Result Id : {} for Job Id : {} ", resultId, jobId);
				repositoryService.updateAcuityScoreMetadataJobStatusBySfStatus(ResourceJobStatusEnum.SF_SUCCESS.getStatus(), resultId, new Date(), jobId);
			} else {					
				failureMessage = s.getErrors()[0].getMessage();
				logger.info("SF Failed Message : {} for Job Id : {} ", failureMessage, jobId);
				repositoryService.updateAcuityScoreMetadataJobStatusBySfFailureStatus(ResourceJobStatusEnum.SF_FAILED.getStatus(), failureMessage, new Date(), jobId);
			}

			i++;
		}
	}
}
