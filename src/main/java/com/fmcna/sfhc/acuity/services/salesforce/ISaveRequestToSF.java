package com.fmcna.sfhc.acuity.services.salesforce;

import java.util.List;

import com.fmcna.sfhc.acuity.persistence.jobs.beans.AcuityScoreMetadataJobStatus;
import com.fmcna.sfhc.acuity.services.exception.ServiceException;
import com.sforce.soap.partner.UpsertResult;

public interface ISaveRequestToSF {
	UpsertResult[] saveSocialWorkerAcuityScoresToSf(List<AcuityScoreMetadataJobStatus> patientMetadataList) throws ServiceException;
	
	UpsertResult[] saveDieticianAcuityScoresToSf(List<AcuityScoreMetadataJobStatus> socialWorkerMetadataList) throws ServiceException;
}