package com.fmcna.sfhc.acuity.services.scheduler;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fmcna.sfhc.acuity.beans.enums.ResourceJobStatusEnum;
import com.fmcna.sfhc.acuity.persistence.jobs.beans.AcuityScoreMetadataJobStatus;
import com.fmcna.sfhc.acuity.services.RepositoryService;
import com.fmcna.sfhc.acuity.services.SalesforceService;

@Component
public class RetryScheduler {
	private static final Logger logger = LoggerFactory.getLogger(RetryScheduler.class);

	@Value("${enable.sw.retry.processing.job}")
	private boolean isSwRetryProcessingEnabled;
	
	@Value("${enable.rd.retry.processing.job}")
	private boolean isRdRetryProcessingEnabled;
	
	@Autowired
	private RepositoryService repositoryService;
	
	@Autowired
	private SalesforceService salesforceService;
	
	@Scheduled(cron = "${sw.retry.processing.scheduler.cron}")
	public void processSwAcuityScoresToSf() {
		if (isSwRetryProcessingEnabled) {
			this.handleSocialWorkerFailedFhirJobs();
		}
	}
	
	@Scheduled(cron = "${rd.retry.processing.scheduler.cron}")
	public void processRdAcuityScoresToSf() {
		if (isRdRetryProcessingEnabled) {
			this.handleDieticianFailedFhirJobs();
		}
	}
	
	private void handleSocialWorkerFailedFhirJobs() {
		List<AcuityScoreMetadataJobStatus> swMetadataList = null;
		try {
			swMetadataList = repositoryService.getAcuityScoreMetadataByStatusTypeDate(
					Arrays.asList(ResourceJobStatusEnum.FHIR_SENT.getStatus()), "SW_JOB", new Date());

			if (swMetadataList == null) {
				logger.info("UNABLE TO FETCH SOCIAL WORK FHIR_SENT (FAILED) METADATA FROM DATABASE");
			} else if (swMetadataList.size() == 0) {
				logger.info("NO SOCIAL WORKER METADATA RECORDS EXIST WITH FHIR_SENT (FAILED) STATUS TO BE MIGRATED IN THIS SCHEDULE");
			} else {
				logger.info("[TOTAL SOCIAL WORKER FHIR_SENT (FAILED) METADATA RECORDS TO BE PROCESSED BY SCHEDULER] ->> " + swMetadataList.size());
				salesforceService.sendSocialWorkerAcuityScoresToSalesforce(swMetadataList);
			}
		} catch (Exception ex) {
			logger.error("Error occurred in RetryScheduler:::handleSocialWorkerFailedFhirJobs() ==>>> " + ex.getMessage());
			ex.printStackTrace();
		}
	}
	
	private void handleDieticianFailedFhirJobs() {
		List<AcuityScoreMetadataJobStatus> rdMetadataList = null;
		try {
			rdMetadataList = repositoryService.getAcuityScoreMetadataByStatusTypeDate(
					Arrays.asList(ResourceJobStatusEnum.FHIR_SENT.getStatus()), "RD_JOB", new Date());

			if (rdMetadataList == null) {
				logger.info("UNABLE TO FETCH DIETICIAN FHIR_SENT (FAILED) METADATA FROM DATABASE");
			} else if (rdMetadataList.size() == 0) {
				logger.info("NO DIETICIAN METADATA RECORDS EXIST WITH FHIR_SENT (FAILED) STATUS TO BE MIGRATED IN THIS SCHEDULE");
			} else {
				logger.info("[TOTAL DIETICIAN FHIR_SENT (FAILED) METADATA RECORDS TO BE PROCESSED BY SCHEDULER] ->> " + rdMetadataList.size());
				salesforceService.sendDieticianAcuityScoresToSalesforce(rdMetadataList);
			}
		} catch (Exception ex) {
			logger.error("Error occurred in RetryScheduler:::handleDieticianFailedFhirJobs() ==>>> " + ex.getMessage());
			ex.printStackTrace();
		}
	} 
}
