package com.fmcna.sfhc.acuity.services;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fmcna.sfhc.acuity.persistence.acuity.repo.AcuityScoreStagingDAO;
import com.fmcna.sfhc.acuity.persistence.jobs.beans.AcuityScoreMetadataJobStatus;
import com.fmcna.sfhc.acuity.persistence.jobs.beans.AcuityScoreMigrationStatus;
import com.fmcna.sfhc.acuity.persistence.jobs.repo.JobStatusStagingDAO;

@Service
@Scope("prototype")
public class RepositoryService {
	
	@Autowired
	private AcuityScoreStagingDAO acuityScoreStagingDao;
	
	@Autowired
	private JobStatusStagingDAO jobStatusStagingDao;
	
	public AcuityScoreMigrationStatus saveMigrationJob(AcuityScoreMigrationStatus migrationJob) {

		return jobStatusStagingDao.saveMigrationJob(migrationJob);
	}
	
	public AcuityScoreMetadataJobStatus saveAcuityScoreMetadata(AcuityScoreMetadataJobStatus metadataJob) {

		return jobStatusStagingDao.saveAcuityScoreMetadata(metadataJob);
	}
	
	public List<AcuityScoreMetadataJobStatus> saveAcuityScoreMetadataList(List<AcuityScoreMetadataJobStatus> metadataList) {
		
		return jobStatusStagingDao.saveAcuityScoreMetadataList(metadataList);
	}
	
	public List<AcuityScoreMetadataJobStatus> getAcuityScoreMetadataByStatus(List<String> jobStatus, String jobType) {
		
		return jobStatusStagingDao.getAcuityScoreMetadataByStatus(jobStatus, jobType);
	}
	
	public List<AcuityScoreMetadataJobStatus> getAcuityScoreMetadataByStatusTypeDate(List<String> jobStatus, String jobType, Date createdDate) {
		
		return jobStatusStagingDao.getAcuityScoreMetadataByStatusTypeDate(jobStatus, jobType, createdDate);
	}
	
	public Integer updateAcuityScoreMetadataByFhirId(String jobStatus, String fhirId, Date modifiedDate, Long mrn, Long jobId) {
		
		return jobStatusStagingDao.updateAcuityScoreMetadataByFhirId(jobStatus, fhirId, modifiedDate, mrn, jobId);
	}
	
	public Integer updateAcuityScoreMetadataJobStatusByFhirStatus(String jobStatus, Date modifiedDate, String fhirId, Date predictionDate) {
		
		return jobStatusStagingDao.updateAcuityScoreMetadataJobStatusByFhirStatus(jobStatus, modifiedDate, fhirId, predictionDate);
	}
	
	public Integer updateAcuityScoreMetadataJobStatusBySfStatus(String jobStatus, String sfId, Date modifiedDate, Long jobId) {
		
		return jobStatusStagingDao.updateAcuityScoreMetadataJobStatusBySfStatus(jobStatus, sfId, modifiedDate, jobId);
	}
	
	public Integer updateAcuityScoreMetadataJobStatusBySfFailureStatus(String jobStatus, String failureReason, Date modifiedDate, Long jobId) {
		
		return jobStatusStagingDao.updateAcuityScoreMetadataJobStatusBySfFailureStatus(jobStatus, failureReason, modifiedDate, jobId);
	}
	
	public Integer updateAcuityScoreMetadataJobStatusByJobStatus(String jobStatus, Date modifiedDate, List<Long> jobIds) {
		
		return jobStatusStagingDao.updateAcuityScoreMetadataJobStatusByJobStatus(jobStatus, modifiedDate, jobIds);
	}
	
	public ResultSet getSocialWorkerModifiedAcuityScores(LocalDate predictionDate) {
		
		return acuityScoreStagingDao.getSocialWorkerModifiedAcuityScores(predictionDate);
	}
	
	public ResultSet getDieticianModifiedAcuityScores(LocalDate predictionDate) {

		return acuityScoreStagingDao.getDieticianModifiedAcuityScores(predictionDate);
	}
	
	public ResultSet getSocialWorkerAcuityScoreMetadata(LocalDate predictionDate) {
		
		return acuityScoreStagingDao.getSocialWorkerAcuityScoreMetadata(predictionDate);
	}
	
	public ResultSet getDieticianAcuityScoreMetadata(LocalDate predictionDate) {
		
		return acuityScoreStagingDao.getDieticianAcuityScoreMetadata(predictionDate);
	}
	
	public ResultSet getSocialWorkerModifiedAcuityScoresByMrnList(LocalDate predictionDate, String mrnList) {
		return acuityScoreStagingDao.getSocialWorkerModifiedAcuityScoresByMrnList(predictionDate, mrnList);
	}
	
	public ResultSet getDieticianModifiedAcuityScoresByMrnList(LocalDate predictionDate, String mrnList) {
		return acuityScoreStagingDao.getDieticianModifiedAcuityScoresByMrnList(predictionDate, mrnList);
	}
	
	public ResultSet getSocialWorkerAcuityScoreMetadataByMrnList(LocalDate predictionDate, String mrnList) {
		return acuityScoreStagingDao.getSocialWorkerAcuityScoreMetadataByMrnList(predictionDate, mrnList);
	}
	
	public ResultSet getDieticianAcuityScoreMetadataByMrnList(LocalDate predictionDate, String mrnList) {
		return acuityScoreStagingDao.getDieticianAcuityScoreMetadataByMrnList(predictionDate, mrnList);
	}
}