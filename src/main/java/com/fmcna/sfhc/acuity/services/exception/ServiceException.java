package com.fmcna.sfhc.acuity.services.exception;

public class ServiceException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -4940404552909338634L;
	private String errCode;

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public ServiceException(String errCode, String message) {
        super(message);
        this.errCode = errCode;
    }

    public ServiceException(String message) {
    	 super(message);
    }

    public ServiceException(final String message, final Throwable t) {
        super(message, t);
    }
}
