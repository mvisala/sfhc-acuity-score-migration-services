package com.fmcna.sfhc.acuity.services.scheduler;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fmcna.sfhc.acuity.beans.enums.ResourceJobStatusEnum;
import com.fmcna.sfhc.acuity.persistence.jobs.beans.AcuityScoreMetadataJobStatus;
import com.fmcna.sfhc.acuity.processor.DieticianMetadataProcessor;
import com.fmcna.sfhc.acuity.processor.SocialWorkerMetadataProcessor;
import com.fmcna.sfhc.acuity.services.RepositoryService;
import com.fmcna.sfhc.acuity.services.SalesforceService;

@Component
public class SchedulerService {
	private static final Logger logger = LoggerFactory.getLogger(SchedulerService.class);

	@Value("${enable.bulk.metadata.migration.processing}")
	private boolean bulkMigrationEnabled;
	
	@Value("${enable.sw.migration.processing}")
	private boolean swSchedulerEnabled;
	
	@Value("${enable.rd.migration.processing}")
	private boolean rdSchedulerEnabled;
	
	@Autowired
	private RepositoryService repositoryService;
	
	@Autowired
	private DieticianMetadataProcessor rdMetadataProcessor;
	
	@Autowired
	private SocialWorkerMetadataProcessor swMetadataProcessor;
	
	@Autowired
	private SalesforceService salesforceService;
	
	@Scheduled(cron = "${sw.acuity.scores.migration.cron}")
	public void saveSwAcuityScoresScheduler() {
		if (swSchedulerEnabled) {
			logger.info("***** SW Bulk Migration Job started for Social Worker Acuity Scores Migration ******");
			LocalDate currentDate = java.time.LocalDate.now();
			ResultSet resultSet = repositoryService.getSocialWorkerModifiedAcuityScores(currentDate);
			swMetadataProcessor.saveSocialWorkerMetadata(resultSet);
		}
	}
	
	@Scheduled(cron = "${rd.acuity.scores.migration.cron}")
	public void saveRdAcuityScoresScheduler() {
		if (rdSchedulerEnabled) {
			logger.info("***** RD Bulk Migration Job started for Registered Dieticians Acuity Scores Migration ******");
			LocalDate currentDate = java.time.LocalDate.now();
			ResultSet resultSet = repositoryService.getDieticianModifiedAcuityScores(currentDate);
			rdMetadataProcessor.saveDieticianMetadata(resultSet);
		}
	}
	
	@Scheduled(cron = "${bulk.metadata.migration.processing.cron}")
	public void sendAcuityScoresToSalesforce() {
		if (bulkMigrationEnabled) {
			this.rdAcuityScoreBulkMigrationToSF();
			this.swAcuityScoreBulkMigrationToSF();
		}
	}
	
	public void swAcuityScoreBulkMigrationToSF() {
		List<AcuityScoreMetadataJobStatus> swMetadataList = null;
		try {
			logger.info("Going to fetch [FHIR_SUCCESS] sw jobs from metadata table to migrate SW scores to Salesforce.....");
			swMetadataList = repositoryService.getAcuityScoreMetadataByStatus(
					Arrays.asList(ResourceJobStatusEnum.FHIR_SUCCESS.getStatus()), "SW_JOB");

			if (swMetadataList == null) {
				logger.info("UNABLE TO FETCH SOCIAL WORK METADATA FROM DATABASE");
			} else if (swMetadataList.size() == 0) {
				logger.info("NO SOCIAL WORKER METADATA RECORDS EXIST WITH [FHIR_SUCCESS] STATUS TO BE MIGRATED IN THIS SCHEDULE");
			} else {
				logger.info("[TOTAL SOCIAL WORKER METADATA RECORDS TO BE PROCESSED BY SCHEDULER] ->> " + swMetadataList.size());
				salesforceService.sendSocialWorkerAcuityScoresToSalesforce(swMetadataList);
			}
		} catch (Exception ex) {
			logger.error("Error occurred in SchedulerService:::swAcuityScoreBulkMigrationToSF() ==>>> " + ex.getMessage());
			ex.printStackTrace();
		}
	}
	
	public void rdAcuityScoreBulkMigrationToSF() {
		List<AcuityScoreMetadataJobStatus> rdMetadataList = null;
		try {
			logger.info("Going to fetch [FHIR_SUCCESS] rd jobs from metadata table to migrate RD scores to Salesforce.....");
			rdMetadataList = repositoryService.getAcuityScoreMetadataByStatus(
					Arrays.asList(ResourceJobStatusEnum.FHIR_SUCCESS.getStatus()), "RD_JOB");

			if (rdMetadataList == null) {
				logger.info("UNABLE TO FETCH DIETICIAN METADATA FROM DATABASE");
			} else if (rdMetadataList.size() == 0) {
				logger.info("NO DIETICIAN METADATA RECORDS EXIST WITH [FHIR_SUCCESS] STATUS TO BE MIGRATED IN THIS SCHEDULE");
			} else {
				logger.info("[TOTAL DIETICIAN METADATA RECORDS TO BE PROCESSED BY SCHEDULER] ->> " + rdMetadataList.size());
				salesforceService.sendDieticianAcuityScoresToSalesforce(rdMetadataList);
			}
		} catch (Exception ex) {
			logger.error("Error occurred in SchedulerService:::rdAcuityScoreBulkMigrationToSF() ==>>> " + ex.getMessage());
			ex.printStackTrace();
		}
	} 
}
