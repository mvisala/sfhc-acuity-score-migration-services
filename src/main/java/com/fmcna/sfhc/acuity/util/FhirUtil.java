package com.fmcna.sfhc.acuity.util;

import java.util.Date;

import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;

import com.fmcna.sfhc.acuity.constants.AcuityConstants;

public class FhirUtil {
	
	public static CodeableConcept getCodeableConcept(String code, String display, String system) {
		CodeableConcept codeableConcept = new CodeableConcept();
		Coding coding = new Coding();
		if (code != null)
			coding.setCode(code);
		if (system != null)
			coding.setSystem(system);
		if (display != null)
			coding.setDisplay(display);
		//codeableConcept.getCoding().add(coding);
		codeableConcept.addCoding(coding);
		return codeableConcept;
	}
	
	//To generate Identifier for Measure Report
	public static String generateReferenceId(String resourceId, String acuityType) {
		/*int randomNumber = 1;
		try {
			SecureRandom number = SecureRandom.getInstance("SHA1PRNG");
			randomNumber = number.nextInt(1000000000) + 1;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} */
		
		StringBuffer sb = new StringBuffer();
		//sb.append(resourceId).append(AcuityConstants.FHIR_DATEFORMAT_YYYYMMDD.format(new Date())).append(acuityType);
		sb.append(resourceId).append(acuityType);
		
		return sb.toString();
	}
	
	public static String generateBundleId(String resource, Date predictionDate) {
		StringBuffer sb = new StringBuffer();
		sb.append("ACUITYRDBUNDLE").append(resource).append(AcuityConstants.FHIR_DATEFORMAT_YYYYMMDDHHHMMMSss.format(predictionDate));
		
		return sb.toString();
	}
}
