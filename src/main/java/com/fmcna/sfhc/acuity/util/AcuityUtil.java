package com.fmcna.sfhc.acuity.util;

import java.io.BufferedReader;
import java.sql.Clob;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AcuityUtil {
	private static Logger logger = LoggerFactory.getLogger(AcuityUtil.class);

	/**
	 * get todays formatted date
	 * 
	 * @return
	 */
	public static String getTodaysDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
		String dataString = dateFormat.format(date);
		logger.info("todays Date : " + dataString);

		return dataString;
	}
	
	public static Date getFormattedDate(String dateString) {
		//DateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = simpleDateFormat.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return date;
	}

	public static Date getCurrentDateObject() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	 
	    return calendar.getTime();
	}

	public static String getClobAsString(Clob data) {
		String line;
		StringBuilder sb = new StringBuilder();
		try {
	        BufferedReader br = new BufferedReader(data.getCharacterStream());
	        
	        while(null != (line = br.readLine())) {
	            sb.append(line);
	        }
	        br.close();
		} catch (Exception ex) {
			logger.error("Error occurred while converting Clob to String -->> " + ex.getMessage());
			ex.printStackTrace();
		}
		
        return sb.toString();
	}
	
	public static String getMrnList(String mrns) {
		
		String mrnList = null;
		if (mrns != null) {
			String[] mrnArray = mrns.split(",");
			List<String> list = new ArrayList<>();

			for (int i = 0; i < mrnArray.length; i++) {
				list.add("'" + mrnArray[i] + "'");
			}

			mrnList = String.join(",", list);
		}
		return mrnList;
	}
	
	public static <X> void setIfNotNull(Consumer<X> setter, Supplier<X> getter) {
	    X value = getter.get();
	    if (value != null) {
	        setter.accept(value);
	    }
	}
}
