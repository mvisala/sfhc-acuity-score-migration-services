package com.fmcna.sfhc.acuity.controller;

import java.sql.ResultSet;
import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.fmcna.sfhc.acuity.constants.AcuityConstants;
import com.fmcna.sfhc.acuity.processor.DieticianMetadataProcessor;
import com.fmcna.sfhc.acuity.processor.SocialWorkerMetadataProcessor;
import com.fmcna.sfhc.acuity.services.RepositoryService;
import com.fmcna.sfhc.acuity.services.scheduler.SchedulerService;
import com.fmcna.sfhc.acuity.util.AcuityUtil;

@RestController
public class AcuityScoreDataController {
	
	@Autowired
	private RepositoryService repositoryService;
	
	@Autowired
	private DieticianMetadataProcessor rdMetadataProcessor;
	
	@Autowired
	private SocialWorkerMetadataProcessor swMetadataProcessor;
	
	@Autowired
	private SchedulerService schedulerService;
	
	private static final Logger logger = LoggerFactory.getLogger(AcuityScoreDataController.class);
	
	@GetMapping("/migrateallscores/{predictionDate}/{acuityType}") ///acuity/migrateallscores/2019-12-16/rd
	public String migrateAcuityScores(@PathVariable String predictionDate, @PathVariable String acuityType) {
		logger.info("Invoking /migrateallscores/" +predictionDate+ "/"+acuityType);
		String status = null;
		try {
			int migrationCount = 0;
			ResultSet resultSet = null;
			LocalDate date = LocalDate.parse(predictionDate.toString());
			
			if (acuityType.equalsIgnoreCase(AcuityConstants.FMC_ACUITY_TYPE_SW_RESOURCE)) {
				resultSet = repositoryService.getSocialWorkerAcuityScoreMetadata(date);
				migrationCount = swMetadataProcessor.saveSocialWorkerMetadata(resultSet);
			} else if (acuityType.equalsIgnoreCase(AcuityConstants.FMC_ACUITY_TYPE_RD_RESOURCE)) {
				resultSet = repositoryService.getDieticianAcuityScoreMetadata(date);
				migrationCount = rdMetadataProcessor.saveDieticianMetadata(resultSet);
			}
			if (migrationCount > 0) {
				status = "Successfully migrated  " +migrationCount+ " acuity scores";
			}
		} catch (Exception e) {
			status = "Unable to migrate acuity scores -> " +e.getMessage();
			logger.error(status);
			e.printStackTrace();
		}
		return status;
	}
	
	@GetMapping("/migratemodifiedscores/{predictionDate}/{acuityType}")
	public String migrateModifiedAcuityScores(@PathVariable String predictionDate, @PathVariable String acuityType) {
		logger.info("Invoking /migratemodifiedscores/" +predictionDate+ "/"+acuityType);
		String status = null;
		try {
			int migrationCount = -1;
			ResultSet resultSet = null;
			LocalDate date = LocalDate.parse(predictionDate.toString());;
			if (acuityType.equalsIgnoreCase(AcuityConstants.FMC_ACUITY_TYPE_SW_RESOURCE)) {
				resultSet = repositoryService.getSocialWorkerModifiedAcuityScores(date);
				migrationCount = swMetadataProcessor.saveSocialWorkerMetadata(resultSet);
			} else if (acuityType.equalsIgnoreCase(AcuityConstants.FMC_ACUITY_TYPE_RD_RESOURCE)) {
				resultSet = repositoryService.getDieticianModifiedAcuityScores(date);
				migrationCount = rdMetadataProcessor.saveDieticianMetadata(resultSet);
			}
			if (migrationCount > 0) {
				status = "Successfully migrated  " +migrationCount+ " modified acuity scores";
			} else if (migrationCount ==0) {
				status = "No acuity scores were modified to migrate";
			}
		} catch (Exception e) {
			status = "Unable to migrate modified acuity scores -> " +e.getMessage();
			logger.error(status);
			e.printStackTrace();
		}
		return status;
	}
	
	@GetMapping("/migratelist/{predictionDate}/{acuityType}/{mrns}")
	public String migrateMetadataList(@PathVariable String predictionDate, 
			@PathVariable String acuityType, @PathVariable String mrns) {
		logger.info("Invoking /migratelist/" +predictionDate+ "/"+acuityType);
		String status = null;
		try {
			int extractCount = 0;
			ResultSet resultSet = null;
			String mrnList = AcuityUtil.getMrnList(mrns);
			LocalDate date = LocalDate.parse(predictionDate.toString());
			
			if (mrnList != null) {
				if (acuityType.equalsIgnoreCase(AcuityConstants.FMC_ACUITY_TYPE_SW_RESOURCE)) {
					resultSet = repositoryService.getSocialWorkerAcuityScoreMetadataByMrnList(date, mrnList);
					extractCount = swMetadataProcessor.saveSocialWorkerMetadata(resultSet);
				} else if (acuityType.equalsIgnoreCase(AcuityConstants.FMC_ACUITY_TYPE_RD_RESOURCE)) {
					resultSet = repositoryService.getDieticianAcuityScoreMetadataByMrnList(date, mrnList);
					extractCount = rdMetadataProcessor.saveDieticianMetadata(resultSet);
				}
				if (extractCount > 0) {
					status = "Total records migrated : " + extractCount;
				}
			} else {
				logger.error("Mrn List is null ");
				status =  AcuityConstants.mrnListError;
			}
		} catch (Exception e) {
			status =  AcuityConstants.mrnListError;
			logger.error(status + " -> " +e.getMessage());
			e.printStackTrace();
		}
		
		return status;
	}
	
	@GetMapping("/migratemodifiedlist/{predictionDate}/{acuityType}/{list}/{mrns}")
	public String migrateModifiedMetadataList(@PathVariable String predictionDate, @PathVariable String acuityType, @PathVariable String mrns) {
		String status = null;
		logger.info("Invoking /migratemodifiedlist/" +predictionDate+ "/"+acuityType);
		
		try {
			ResultSet resultSet = null;
			int migrationCount = 0;
			String mrnList = AcuityUtil.getMrnList(mrns);
			LocalDate date = LocalDate.parse(predictionDate.toString());
			
			if (mrnList != null) {
				if (acuityType.equalsIgnoreCase(AcuityConstants.FMC_ACUITY_TYPE_SW_RESOURCE)) {
					resultSet = repositoryService.getSocialWorkerModifiedAcuityScoresByMrnList(date, mrnList);
					migrationCount = swMetadataProcessor.saveSocialWorkerMetadata(resultSet);
				} else if (acuityType.equalsIgnoreCase(AcuityConstants.FMC_ACUITY_TYPE_RD_RESOURCE)) {
					resultSet = repositoryService.getDieticianModifiedAcuityScoresByMrnList(date, mrnList);
					migrationCount = rdMetadataProcessor.saveDieticianMetadata(resultSet);
				}
				
				if (migrationCount >0) {
					status = "Total records migrated : " + migrationCount;
				}
			} else {
				logger.error(" Mrn List is null ");
				status = "Please provide valid MRN list with comma...";
			}
		} catch (Exception e) {
			status = "Please provide valid MRN list with comma... ";
			logger.error(status + " -> " +e.getMessage());
			e.printStackTrace();
		}
		
		return status;
	}
	
	@GetMapping("/migratescorestosf/{acuityType}") ///acuity/migratescorestosf/
	public String migrateAcuityScoresToSalesforce(@PathVariable String acuityType) {
		logger.info("Invoking /migratescorestosf/"+acuityType);
		String status = null;
		try {
			if (acuityType.equalsIgnoreCase(AcuityConstants.FMC_ACUITY_TYPE_SW_RESOURCE)) {
				schedulerService.swAcuityScoreBulkMigrationToSF();
			} else if (acuityType.equalsIgnoreCase(AcuityConstants.FMC_ACUITY_TYPE_RD_RESOURCE)) {
				schedulerService.rdAcuityScoreBulkMigrationToSF();
			}
			
		} catch (Exception e) {
			status = "Unable to migrate acuity scores to sfhc -> " +e.getMessage();
			logger.error(status);
			e.printStackTrace();
		}
		return status;
	}
	
	@GetMapping("/getextractcount/{capacity}/{predictionDate}/{acuityType}")
	public String getExtractCount(@PathVariable String capacity, @PathVariable String predictionDate,
			@PathVariable String acuityType) {
		logger.info("Invoking /getextractcount/" + predictionDate + "/" + acuityType);
		String status = null;
		try {
			int extractCount = 0;
			ResultSet resultSet = null;
			LocalDate date = LocalDate.parse(predictionDate.toString());
			
			if (capacity != null && capacity.equalsIgnoreCase("allscores")) {
				if (acuityType.equalsIgnoreCase(AcuityConstants.FMC_ACUITY_TYPE_SW_RESOURCE)) {
					resultSet = repositoryService.getSocialWorkerAcuityScoreMetadata(date);
				} else if (acuityType.equalsIgnoreCase(AcuityConstants.FMC_ACUITY_TYPE_RD_RESOURCE)) {
					resultSet = repositoryService.getDieticianAcuityScoreMetadata(date);
				}
			} else if (capacity != null && capacity.equalsIgnoreCase("modifiedscores")) {
				if (acuityType.equalsIgnoreCase(AcuityConstants.FMC_ACUITY_TYPE_SW_RESOURCE)) {
					resultSet = repositoryService.getSocialWorkerModifiedAcuityScores(date);
				} else if (acuityType.equalsIgnoreCase(AcuityConstants.FMC_ACUITY_TYPE_RD_RESOURCE)) {
					resultSet = repositoryService.getDieticianModifiedAcuityScores(date);
				}
			}

			if (resultSet != null) {
				resultSet.last();
				extractCount = resultSet.getRow();
				status = "Total Fetch Size ::: " + extractCount;
			} else {
				status = "Resultset is null";
			}
			logger.info(status);
		} catch (Exception e) {
			status = "Unable to extract acuity scores -> " + e.getMessage();
			logger.error(status);
			e.printStackTrace();
		}

		return status;
	}
}
