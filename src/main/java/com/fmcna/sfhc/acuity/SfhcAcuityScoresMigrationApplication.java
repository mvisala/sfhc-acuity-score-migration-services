package com.fmcna.sfhc.acuity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@SpringBootApplication
@EnableScheduling
@PropertySource("file:/apps/common/sbootapps/sf-services/sfhc-acuity-score-migration-services/config/sfhc-acuity-score-migration-services.properties")
public class SfhcAcuityScoresMigrationApplication {

	private static Logger logger = LoggerFactory.getLogger(SfhcAcuityScoresMigrationApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SfhcAcuityScoresMigrationApplication.class, args);
		
		logger.info("********* Successfully loaded ::: sfhc-acuity-score-migration-services ::: SpringBoot Application ********");
		
		//logger.info("Available processors size -> " + Runtime.getRuntime().availableProcessors());

		//logger.info("Targeted parallelism level of the ForkJoinPool -> " + ForkJoinPool.getCommonPoolParallelism());
	}
	
	/*@Bean("cachedThreadPool")
	public ExecutorService cachedThreadPool() {
		return Executors.newCachedThreadPool();
	} */
}
