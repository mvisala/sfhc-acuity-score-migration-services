package com.fmcna.sfhc.acuity.fhir;

import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import com.fmcna.sfhc.acuity.services.mapper.FHIRRequestMapper;

@Service
public class KafkaProducer {

	private static Logger logger = LoggerFactory.getLogger(KafkaProducer.class);

	@Value("${kafka.topic.ingress.bundle}")
	private String bundleIngressTopic;

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	
	@Autowired
	FHIRRequestMapper fhirRequestMapper;
	
	public boolean sendMeasureReportBundle(String fhirMeasureReportBundleMessage) {
		boolean hasOffset = false;
		RecordMetadata recordMetadata = null;
		ListenableFuture<SendResult<String, String>> kafkaResponse = this.kafkaTemplate.send(bundleIngressTopic, fhirMeasureReportBundleMessage);

		try {
			recordMetadata = kafkaResponse.get().getRecordMetadata();
			logger.info("BUNDLE successfully pushed to topic: {} with offset: {} and partition: {}", bundleIngressTopic,
					recordMetadata.offset(), recordMetadata.partition());
			
			hasOffset =  recordMetadata.hasOffset();
		} catch (InterruptedException | ExecutionException e) {
			logger.error("InterruptedException | ExecutionException while sending BUNDLE to topic : " + bundleIngressTopic + ":" + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("Exception while sending BUNDLE to topic : " + bundleIngressTopic + ":" + e.getMessage());
			e.printStackTrace();
		}
		return hasOffset;
	}
}