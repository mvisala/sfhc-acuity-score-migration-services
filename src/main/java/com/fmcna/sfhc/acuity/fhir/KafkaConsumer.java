package com.fmcna.sfhc.acuity.fhir;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import com.fmcna.sfhc.acuity.services.mapper.FHIRResponseMapper;

@Service
public class KafkaConsumer {

	@Autowired
	FHIRResponseMapper fhirResponseMapper;

	@KafkaListener(topics = "${kafka.topic.outgress.measurereport}") //JPA_MEASUREREPORT
    public void receiveMeasureReport(@Payload String message, @Headers MessageHeaders headers) {
		
        fhirResponseMapper.processMeasureReport(message, headers);
    }
}