package com.fmcna.sfhc.acuity.configuration.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class AcuityDbConnection {
	
	@Value("${spring.acuitydatasource.jdbc-url}")
	private String dbUrl;
	
	@Value("${spring.acuitydatasource.username}")
	private String dbUser;
	
	@Value("${spring.acuitydatasource.password}")
	private String dbPwd;
	
	@Bean(name = "acuityConnection")
	public Connection getConnection() {
		
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(dbUrl, dbUser, dbPwd);
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return connection;
	}
}