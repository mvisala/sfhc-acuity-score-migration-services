package com.fmcna.sfhc.acuity.configuration;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import com.fmcna.sfhc.acuity.fhir.KafkaProducer;

@Configuration
@EnableKafka
public class FHIRKafkaSenderConfig {

	@Value("${kafka.brokers}")
	private String bootstrapServers;

	@Value("${kafka.retry.count}")
	private Integer retryCount;
	
	@Value("${kafka.ssl.truststore.location}")
	private String truststoreLocation;
	
	@Value("${kafka.ssl.truststore.password}")
	private String truststorePassword;
	
	@Value("${kafka.ssl.keystore.location}")
	private String keystoreLocation;
	
	@Value("${kafka.ssl.keystore.password}")
	private String keystorePassword;
	
	@Value("${kafka.ssl.key.password}")
	private String keyPassword;
	
	@Bean
	  public Map<String, Object> producerConfigs() {
	    Map<String, Object> properties = new HashMap<>();
	    properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
	    properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
	    properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		properties.put("security.protocol", "SSL");
		properties.put("ssl.truststore.location", truststoreLocation);
		properties.put("ssl.truststore.password", truststorePassword);
		properties.put("ssl.keystore.location", keystoreLocation);
		properties.put("ssl.keystore.password", keystorePassword);
		properties.put("ssl.key.password", keyPassword);
		properties.put("acks", "all");
		properties.put("retries", retryCount);
		
	    return properties;
	  }

	  @Bean
	  public ProducerFactory<String, String> producerFactory() {
	    return new DefaultKafkaProducerFactory<>(producerConfigs());
	  }

	  @Bean
	  public KafkaTemplate<String, String> kafkaTemplate() {
	    return new KafkaTemplate<>(producerFactory());
	  }

	  @Bean
	  public KafkaProducer sender() {
	    return new KafkaProducer();
	  }

}
