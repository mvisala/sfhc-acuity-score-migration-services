package com.fmcna.sfhc.acuity.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.ServerValidationModeEnum;

@Configuration
public class FHIRConfiguration {

	private FhirContext fhirContext;
	
	@Bean(name = "fhirContext")
	public FhirContext getfhirContext() {
		fhirContext = FhirContext.forDstu3();
		fhirContext.getRestfulClientFactory().setServerValidationMode(ServerValidationModeEnum.NEVER);
		fhirContext.getRestfulClientFactory().setSocketTimeout(180 * 1000);
		fhirContext.getRestfulClientFactory().setConnectTimeout(180 * 1000);
		fhirContext.getRestfulClientFactory().setPoolMaxTotal(100);
		return fhirContext;
	}
}


