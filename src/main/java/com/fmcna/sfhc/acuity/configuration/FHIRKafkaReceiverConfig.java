package com.fmcna.sfhc.acuity.configuration;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;

import com.fmcna.sfhc.acuity.fhir.KafkaConsumer;

@Configuration
@EnableKafka
public class FHIRKafkaReceiverConfig {

	@Value("${kafka.brokers}")
	private String bootstrapServers;

	@Value("${kafka.group.id}")
	private String groupId;

	@Value("${kafka.retry.count}")
	private Integer retryCount;
	
	@Value("${kafka.max.poll.records}")
	private String maxPollRecords;
	
	@Value("${kafka.ssl.truststore.location}")
	private String truststoreLocation;
	
	@Value("${kafka.ssl.truststore.password}")
	private String truststorePassword;
	
	@Value("${kafka.ssl.keystore.location}")
	private String keystoreLocation;
	
	@Value("${kafka.ssl.keystore.password}")
	private String keystorePassword;
	
	@Value("${kafka.ssl.key.password}")
	private String keyPassword;
	
	@Value("${kafka.enable.auto.commit.config}")
	private String enableAutoCommit;
	
	@Value("${kafka.heartbeat.interval.ms.config}")
	private Integer heartbeatInterval;
	
	@Value("${kafka.session.timeout.ms.config}")
	private Integer sessionTimeout;
	
	@Value("${kafka.auto.commit.interval.ms.config}")
	private Integer autoCommitInterval;
	
	@Bean
	public Map<String, Object> consumerConfigs() {
		Map<String, Object> properties = new HashMap<>();
		
		properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		properties.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
		properties.put("security.protocol", "SSL");
		properties.put("ssl.truststore.location",truststoreLocation);
		properties.put("ssl.truststore.password",truststorePassword);
		properties.put("ssl.keystore.location",keystoreLocation);
		properties.put("ssl.keystore.password",keystorePassword);
		properties.put("ssl.key.password",keyPassword);
		properties.put("acks", "all");
		properties.put("retries", retryCount);
		properties.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, maxPollRecords);
		properties.put(ConsumerConfig.HEARTBEAT_INTERVAL_MS_CONFIG, heartbeatInterval);
		properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, enableAutoCommit);
		properties.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, sessionTimeout);
		properties.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, autoCommitInterval);
		
		return properties;
	}

	@Bean
	public ConsumerFactory<String, String> consumerFactory() {
		return new DefaultKafkaConsumerFactory<>(consumerConfigs());
	}

	@Bean
	public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> kafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(consumerFactory());

		return factory;
	}

	@Bean
	public KafkaConsumer receiver() {
		return new KafkaConsumer();
	}

}
