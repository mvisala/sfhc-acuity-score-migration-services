package com.fmcna.sfhc.acuity.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fmcna.sfhc.acuity.services.exception.SalesforceConnectionException;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

@Service
@Scope("prototype")
public class SalesforceConnection {

	private static Logger logger = LoggerFactory.getLogger(SalesforceConnection.class);

	@Value("${salesforce.healthcloud.login.url}")
	private String sfCloudURL;

	@Value("${salesforce.healthcloud.username}")
	private String sfCloudUserName;

	@Value("${salesforce.healthcloud.password}")
	private String sfCloudPassword;
	
	@Value("${salesforce.healthcloud.api.version}")
	private String sfCloudApiVersion;
	
	public PartnerConnection getPartnerConnection() throws SalesforceConnectionException {

		logger.info("SalesforceConnection.getPartnerConnection called.:" + sfCloudURL);

		String soapAuthEndPoint = sfCloudURL + "/services/Soap/u/" + sfCloudApiVersion;

		PartnerConnection connection = null;

		try {
			ConnectorConfig config = new ConnectorConfig();

			config.setUsername(sfCloudUserName);
			config.setPassword(sfCloudPassword);
			config.setAuthEndpoint(soapAuthEndPoint);
			config.setCompression(true);
			config.setTraceMessage(false);
			config.setPrettyPrintXml(true);

			logger.debug("AuthEndpoint: " + soapAuthEndPoint);

			connection = new PartnerConnection(config);

		} catch (ConnectionException ce) {
			logger.error("Error creating connection to Salesforce: {}", ce.getMessage());
			ce.printStackTrace();
			throw new SalesforceConnectionException( "Error creating connection to Salesforce", ce);
		}

		return connection;
	}

}
