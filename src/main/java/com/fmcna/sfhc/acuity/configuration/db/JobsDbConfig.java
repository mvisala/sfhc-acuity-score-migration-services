package com.fmcna.sfhc.acuity.configuration.db;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "jobsEntityManager", 
						transactionManagerRef = "jobsTransactionManager", 
						basePackages = "com.fmcna.sfhc.acuity.persistence.jobs")
public class JobsDbConfig {

	@Primary
	@Bean
	@ConfigurationProperties(prefix = "spring.jobsdatasource")
	public DataSource jobsDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Primary
	@Bean(name = "jobsEntityManager")
	public LocalContainerEntityManagerFactoryBean jobsEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder.dataSource(jobsDataSource()).packages("com.fmcna.sfhc.acuity.persistence.jobs").build();
	}

	@Primary
	@Bean(name = "jobsTransactionManager")
	public PlatformTransactionManager jobsTransactionManager(
			@Qualifier("jobsEntityManager") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}
}